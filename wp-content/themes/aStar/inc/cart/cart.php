<?php

/**
 * Show cart contents / total Ajax
 */

add_filter('woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');
function woocommerce_header_add_to_cart_fragment($fragments)
{
    global $woocommerce;

    ob_start();

?>
    <div class="cart d-flex flex-row align-items-center justify-content-start cart-customlocation">
        <div class="cart_icon">
            <a href="<?php echo wc_get_cart_url(); ?>" title="<?php _e('View your shopping cart'); ?>">
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/bag.png" alt="">
                <div class="cart_num">
                    <?php
                    echo sprintf('%d', WC()->cart->get_cart_contents_count());
                    ?>
                </div>
            </a>
        </div>
        <div class="cart_text">bag </div>
        <div class="cart_price">
            <?php echo WC()->cart->get_cart_total(); ?>
        </div>
    </div>
<?php
    $fragments['div.cart-customlocation'] = ob_get_clean();
    return $fragments;
}


function custom_wc_dropdown_variation_attribute_options($args = array())
{
    $args = wp_parse_args(apply_filters('woocommerce_dropdown_variation_attribute_options_args', $args), array(
        'options' => false,
        'attribute' => false,
        'product' => false,
        'selected' => false,
        'name' => '',
        'id' => '',
        'class' => '',
        'show_option_none' => __('Choose an option', 'woocommerce'),
    ));

    $options = $args['options'];
    $product = $args['product'];
    $attribute = $args['attribute'];
    $name = $args['name'] ? $args['name'] : 'attribute_' . sanitize_title($attribute);
    $id = $args['id'] ? $args['id'] : sanitize_title($attribute);
    $class = $args['class'];
    $show_option_none = $args['show_option_none'] ? true : false;
    $show_option_none_text = $args['show_option_none'] ? $args['show_option_none'] : __('Choose an option', 'woocommerce'); // We'll do our best to hide the placeholder, but we'll need to show something when resetting options. 

    if (empty($options) && !empty($product) && !empty($attribute)) {
        $attributes = $product->get_variation_attributes();
        $options = $attributes[$attribute];
    }

    $html = '<ul id="' . esc_attr($id) . '" class="' . esc_attr($class) . '" name="' . esc_attr($name) . '" data-attribute_name="attribute_' . esc_attr(sanitize_title($attribute)) . '" data-show_option_none="' . ($show_option_none ? 'yes' : 'no') . '">';
    if (!empty($options)) {

        $variationsList = $product->get_available_variations();
        foreach ($variationsList as $value) {
            $variations[] = $value['attributes']['attribute_pa_size'];
        }
        $i = 1;
        foreach ($options as $option) {
            if (in_array($option, $variations)) {
                $html .= '<li class="size_available">
                <input type="radio" id="radio_' . $i . '" value="' . $option . '" name="product_radio" class="regular_radio radio_' . $i . '"> <label for="radio_' . $i . '">' . $option . '</label> 
                </li>';
            } else {
                $html .= '<li>
                <input type="radio" id="radio_' . $i . '" name="product_radio" class="regular_radio radio_' . $i . '" disabled=""><label for="radio_' . $i . '"> ' . $option . ' </label>
                </li>';
            }
            $i++;
        }
    }
    $html .= '</ul>';

    echo apply_filters('woocommerce_dropdown_variation_attribute_options_html', $html, $args);
}

// add_action('woocommerce_cart_coupon', 'custom_woocommerce_empty_cart_button');
function custom_woocommerce_empty_cart_button()
{
    echo '<a href="' . esc_url(add_query_arg('empty_cart', 'yes')) . '" class="button" title="' . esc_attr('Empty Cart', 'woocommerce') . '">' . esc_html('Empty Cart', 'woocommerce') . '</a>';
}

add_action('wp_loaded', 'custom_woocommerce_empty_cart_action', 20);
function custom_woocommerce_empty_cart_action()
{
    if (isset($_GET['empty_cart']) && 'yes' === esc_html($_GET['empty_cart'])) {
        WC()->cart->empty_cart();

        $referer  = wp_get_referer() ? esc_url(remove_query_arg('empty_cart')) : wc_get_cart_url();
        wp_safe_redirect($referer);
    }
}


function custom_wc_cart_totals_shipping_method_label($method)
{
    $label = '';

    if ($method->cost > 0) {
        if (WC()->cart->tax_display_cart == 'excl') {
            $label .= ': ' . wc_price($method->cost);
            if ($method->get_shipping_tax() > 0 && WC()->cart->prices_include_tax) {
                $label .= ' <small class="tax_label">' . WC()->countries->ex_tax_or_vat() . '</small>';
            }
        } else {
            $label .= ': ' . wc_price($method->cost + $method->get_shipping_tax());
            if ($method->get_shipping_tax() > 0 && !WC()->cart->prices_include_tax) {
                $label .= ' <small class="tax_label">' . WC()->countries->inc_tax_or_vat() . '</small>';
            }
        }
    }

    return apply_filters('woocommerce_cart_shipping_method_full_label', $label, $method);
}

?>