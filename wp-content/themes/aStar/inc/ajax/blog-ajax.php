<?php
function add_admin_url()
{
    wp_enqueue_script('blog', get_template_directory_uri() . '/js/blog.js', array('jquery'), '1.0', true);
    wp_localize_script('blog', 'my_ajaxurl', admin_url('admin-ajax.php'));
}
add_action('wp_enqueue_scripts', 'add_admin_url');

/**
 * Functions ajax portfolio
 */
add_action('wp_ajax_load_more', 'load_more_function');
add_action('wp_ajax_nopriv_load_more', 'load_more_function');
function load_more_function()
{
    $next_page = $_GET['currentPage'] + 1;
    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'posts_per_page' => $_GET['post_per_page'],
        'paged' => $next_page,
    );
    $list_post = new WP_Query($args);
    ob_start();
    if ($list_post->have_posts()) {
        $next = 'false';
        while ($list_post->have_posts()) {
            $list_post->the_post();
            get_template_part('template-parts/content', 'blog');
        }
    }
    $html = ob_get_contents();
    if ($next_page < $list_post->max_num_pages) $next = 'true';
    $response = array(
        'message' => 'success',
        'html' => $html,
        'next' => $next
    );
    ob_end_clean();
    wp_send_json_success($response, 200);
    wp_reset_postdata();
    die();
}
