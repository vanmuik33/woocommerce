<?php
function is_new_product($date, $product)
{
    // Get the date for the product published and current date
    $datetime_created  = $product->get_date_created(); // Get product created datetime
    $timestamp_created = $datetime_created->getTimestamp(); // product created timestamp

    $datetime_now      = new WC_DateTime(); // Get now datetime (from Woocommerce datetime object)
    $timestamp_now     = $datetime_now->getTimestamp(); // Get now timestamp

    $time_delta        = $timestamp_now - $timestamp_created; // Difference in seconds
    $days        = $date * 24 * 60 * 60; // 60 days in seconds

    // If the difference is less than 6, apply "NEW" label to product archive.
    if ($time_delta < $days) {
        return true;
    } else return false;
}

function get_product_size_attribute($product)
{
    $sizes = explode(',',$product->get_attribute('Size'));
    return $sizes;
}

function get_product_color_attribute($product)
{
    $color = explode(',',$product->get_attribute('Color'));
    return $color;
}

