<?php

/**
 * Custom query blog post index (template page: blog)
 * 
 */


function custom_query_get_posts($args)
{
    return new WP_Query($args);
}


/* Custom single-product.php
*
*
*/
function custom_single_product()
{
    // remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
    remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
    // remove_action('woocommerce_before_main_content', 'WC_Structured_Data::generate_website_data()', 30);
}
add_action('woocommerce_before_main_content', 'custom_single_product');

/* Custom custom_woocommerce_before_main_content
*
*
*/

function custom_breadcrumb()
{
    $args = array(
        'delimiter' => '',
        'wrap_before' => '<div class="breadcrumbs"><ul class="d-flex flex-row align-items-center justify-content-start">',
        'wrap_after' => '</ul></div>',
        'before' => '<li>',
        'after' => '</li>',
        // 'home' => _x('Home', 'breadcrumb', 'woocommerce')
    );
    return woocommerce_breadcrumb($args);
}
// woocommerce_breadcrumb($args);
add_action('custom_woocommerce_before_main_content', 'custom_breadcrumb', 20);


/* Custom content-single-product.php
*
*
*/
add_action('custom_woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20);
add_action('custom_woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
add_action('custom_woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
add_action('custom_woocommerce_single_variation', 'woocommerce_single_variation_add_to_cart_button', 10);



// Add product function
require('product/products.php');


/**
 * Show cart contents / total Ajax
 */
require('cart/cart.php');


remove_action('woocommerce_cart_collaterals', 'woocommerce_cross_sell_display');
/**
 * WooCommerce Sales Sorting Filter
 * https://lakewood.media/woocommerce-add-sales-filter/
 */
add_filter('woocommerce_get_catalog_ordering_args', 'wcs_get_catalog_ordering_args');
function wcs_get_catalog_ordering_args($args)
{
    $orderby_value = isset($_GET['orderby']) ? wc_clean($_GET['orderby']) : apply_filters('woocommerce_default_catalog_orderby', get_option('woocommerce_default_catalog_orderby'));

    if ('on_sale' == $orderby_value) {
        $args['orderby'] = 'meta_value_num';
        $args['order'] = 'DESC';
        $args['meta_key'] = '_sale_price';
    }
    if ('hot' == $orderby_value) {
        $args['orderby'] = 'meta_value_num';
        $args['order'] = 'DESC';
        $args['meta_key'] = 'total_sales';
    }
    return $args;
}

add_filter('woocommerce_default_catalog_orderby_options', 'wcs_catalog_orderby');
add_filter('woocommerce_catalog_orderby', 'wcs_catalog_orderby');
function wcs_catalog_orderby($sortby)
{
    $sortby['on_sale'] = 'Sort by on sale';
    $sortby['hot'] = 'Sort by hot product';
    return $sortby;
}




/** CUSOM SHIPING METHOD
 *
 * 
 */
// function my_cart_shipping_method() {
//     if (is_checkout()) {
//         return;
//     }
//     wc_get_template('cart/my_cart_shipping.php');
// }
// // remove_action('woocommerce_cart_collaterals','woocommerce_cart_totals');
// add_action('woocommerce_cart_collaterals','my_cart_shipping_method');
function my_wc_cart_totals_coupon_label($coupon, $echo = true)
{
    if (is_string($coupon)) {
        $coupon = new WC_Coupon($coupon);
    }

    $label = apply_filters('woocommerce_cart_totals_coupon_label', sprintf(esc_html__('%s', 'woocommerce'), $coupon->get_code()), $coupon);

    if ($echo) {
        echo $label;
    } else {
        return $label;
    }
}
function my_cart_shipping_method()
{
    if (is_checkout()) {
        return;
    }
    wc_get_template('cart/my_cart_shipping.php');
}
add_action('my_woocommerce_cart_collaterals', 'my_cart_shipping_method');


/** Custom Checkout page
 * 
 * 
 * 
 * 
 */
require_once ('checkout/checkout.php');