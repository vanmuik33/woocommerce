<!-- Home -->
<?php if (is_front_page()) {
?>
    <div class="home">
        <!-- Home Slider -->
        <div class="home_slider_container">
            <div class="owl-carousel owl-theme home_slider">

                <!-- Slide -->
                <div class="owl-item">
                    <div class="background_image" style="background-image:url(<?php bloginfo('stylesheet_directory'); ?>/images/home_slider_1.jpg)"></div>
                    <div class="home_content_container">
                        <div class="home_content">
                            <div class="home_discount d-flex flex-row align-items-end justify-content-start">
                                <div class="home_discount_num">20</div>
                                <div class="home_discount_text">Discount on the</div>
                            </div>
                            <div class="home_title">New Collection</div>
                            <div class="button button_1 home_button trans_200"><a href="<?php echo wc_get_page_permalink('shop');; ?>">Shop NOW!</a></div>
                        </div>
                    </div>
                </div>

                <!-- Slide -->
                <div class="owl-item">
                    <div class="background_image" style="background-image:url(<?php bloginfo('stylesheet_directory'); ?>/images/home_slider_1.jpg)"></div>
                    <div class="home_content_container">
                        <div class="home_content">
                            <div class="home_discount d-flex flex-row align-items-end justify-content-start">
                                <div class="home_discount_num">20</div>
                                <div class="home_discount_text">Discount on the</div>
                            </div>
                            <div class="home_title">New Collection</div>
                            <div class="button button_1 home_button trans_200"><a href="<?php echo wc_get_page_permalink('shop');; ?>">Shop NOW!</a></div>
                        </div>
                    </div>
                </div>

                <!-- Slide -->
                <div class="owl-item">
                    <div class="background_image" style="background-image:url(<?php bloginfo('stylesheet_directory'); ?>/images/home_slider_1.jpg)"></div>
                    <div class="home_content_container">
                        <div class="home_content">
                            <div class="home_discount d-flex flex-row align-items-end justify-content-start">
                                <div class="home_discount_num">20</div>
                                <div class="home_discount_text">Discount on the</div>
                            </div>
                            <div class="home_title">New Collection</div>
                            <div class="button button_1 home_button trans_200"><a href="<?php echo wc_get_page_permalink('shop');; ?>">Shop NOW!</a></div>
                        </div>
                    </div>
                </div>

            </div>

            <!-- Home Slider Navigation -->
            <div class="home_slider_nav home_slider_prev trans_200">
                <div class=" d-flex flex-column align-items-center justify-content-center"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/prev.png" alt=""></div>
            </div>
            <div class="home_slider_nav home_slider_next trans_200">
                <div class=" d-flex flex-column align-items-center justify-content-center"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/next.png" alt=""></div>
            </div>

        </div>
    </div>
<?php
} elseif (is_archive()) {
?>
    <div class="home">
        <div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="<?php bloginfo('stylesheet_directory'); ?>/images/categories.jpg" data-speed="0.8"></div>
        <div class="home_container">
            <div class="home_content">
                <div class="home_title"><?php woocommerce_page_title() ?></div>
                <?php
                /**
                 * Hook: custom_woocommerce_before_main_content.
                 *
                 * @hooked woocommerce_breadcrumb - 20
                 */
                do_action('custom_woocommerce_before_main_content');
                ?>
            </div>
        </div>
    </div>
<?php
} elseif (is_checkout()) {
?>
    <div class="home">
        <div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="<?php bloginfo('stylesheet_directory'); ?>/images/checkout.jpg" data-speed="0.8"></div>
        <div class="home_container">
            <div class="home_content">
                <div class="home_title"><?php woocommerce_page_title() ?></div>
                <?php
                /**
                 * Hook: custom_woocommerce_before_main_content.
                 *
                 * @hooked woocommerce_breadcrumb - 20
                 */
                do_action('custom_woocommerce_before_main_content');
                ?>
            </div>
        </div>
    </div>
<?php
} else {
?>
    <div class="home">
        <div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="<?php bloginfo('stylesheet_directory'); ?>/images/product_background.jpg" data-speed="0.8"></div>
        <div class="home_container">
            <div class="home_content">
                <div class="home_title"><?php woocommerce_page_title() ?></div>
                <?php
                /**
                 * Hook: custom_woocommerce_before_main_content.
                 *
                 * @hooked woocommerce_breadcrumb - 20
                 */
                do_action('custom_woocommerce_before_main_content');
                ?>
            </div>
        </div>
    </div>
<?php
}
?>