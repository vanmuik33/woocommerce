<?php global $product;
$product_tag = '';

if (is_new_product(6, $product)) $product_tag = 'new';
if ($product->is_featured()) $product_tag = 'hot';
if ($product->is_on_sale()) $product_tag = 'sale';
?>
<!-- Product -->
<div class="product grid-item  <?php echo $product_tag ?>">
    <div class="product_inner">
        <div class="product_image">
            <img style="max-height: 455px;" src="<?php echo the_post_thumbnail_url() ?>" alt="">
            <div class="product_tag"><?php echo $product_tag ?></div>
        </div>
        <div class="product_content text-center">
            <div class="product_title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></div>
            <div class="product_price">
                <?php
                echo '$'.$product->get_price();
                if( $product->is_on_sale()) {
                    echo "<span><del>RDP ".$product->get_regular_price()."</del></span>";
                }
                ?>
            </div>
            <div class="product_button ml-auto mr-auto trans_200"><?php woocommerce_template_loop_add_to_cart(); ?></div>

        </div>
    </div>
</div>