<?php
global $wp;
if (is_archive()) {
    if (!empty($_GET['orderby'])) {
        $orderby = $_GET['orderby'];
    }
    else $orderby = '';
?>
    <!-- Sorting & Filtering -->
    <div class="products_bar">
        <div class="section_container">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="products_bar_content d-flex flex-column flex-xxl-row align-items-start align-items-xxl-center justify-content-start">
                            <div class="product_categories">
                                <ul class="d-flex flex-row align-items-start justify-content-start flex-wrap">
                                    <li <?php if(''==$orderby) echo 'class="active"' ?>><a href="<?php echo home_url($wp->request); ?>">All</a></li>
                                    <li <?php if('hot'==$orderby) echo 'class="active"' ?>><a href="?orderby=hot">Hot Products</a></li>
                                    <li <?php if('date'==$orderby) echo 'class="active"' ?>><a href="?orderby=date">New Products</a></li>
                                    <li <?php if('on_sale'==$orderby) echo 'class="active"' ?>><a href="?orderby=on_sale">Sale Products</a></li>
                                </ul>
                            </div>
                            <div class="products_bar_side ml-xxl-auto d-flex flex-row align-items-center justify-content-start">
                                <div class="products_dropdown product_dropdown_sorting">
                                    <div class="isotope_sorting_text"><span>Default Sorting</span><i class="fa fa-caret-down" aria-hidden="true"></i></div>
                                    <ul>
                                        <li class="item_sorting_btn" data-isotope-option="{ &quot;sortBy&quot;: &quot;original-order&quot; }">Default</li>
                                        <li class="item_sorting_btn" data-isotope-option="{ &quot;sortBy&quot;: &quot;price&quot; }">Price</li>
                                        <li class="item_sorting_btn" data-isotope-option="{ &quot;sortBy&quot;: &quot;name&quot; }">Name</li>
                                    </ul>
                                </div>
                                <div class="product_view d-flex flex-row align-items-center justify-content-start">
                                    <div class="view_item active"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/view_1.png" alt=""></div>
                                    <div class="view_item"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/view_2.png" alt=""></div>
                                    <div class="view_item"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/view_3.png" alt=""></div>
                                </div>
                                <div class="products_dropdown text-right product_dropdown_filter">
                                    <div class="isotope_filter_text"><span>Filter</span><i class="fa fa-caret-down" aria-hidden="true"></i></div>
                                    <ul>
                                        <li class="item_filter_btn" data-filter="*">All</li>
                                        <li class="item_filter_btn" data-filter=".hot">Hot</li>
                                        <li class="item_filter_btn" data-filter=".new">New</li>
                                        <li class="item_filter_btn" data-filter=".sale">Sale</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
} elseif (is_product()) {
?>
    <!-- Sorting & Filtering -->
    <div class="products_bar">
        <div class="section_container">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="products_bar_content d-flex flex-row align-items-center justify-content-start">
                            <div class="product_categories">
                                <ul class="d-flex flex-row align-items-start justify-content-start flex-wrap">
                                    <li class="active"><a href="#">All</a></li>
                                    <li><a href="?orderby=hot">Hot Products</a></li>
                                    <li><a href="?orderby=date">New Products</a></li>
                                    <li><a href="?orderby=on_sale">Sale Products</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
}
?>