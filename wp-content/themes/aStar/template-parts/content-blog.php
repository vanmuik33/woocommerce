<!-- Blog Posts -->
<div class="blog_bosts">

    <!-- Blog Post -->
    <div class="blog_post">
        <div class="blog_post_image"><img src="<?php echo get_the_post_thumbnail_url() ?>" alt=""></div>
        <div class="blog_post_content">
            <div class="blog_post_date"><a href="#"><?php echo get_the_date('F d, Y') ?></a></div>
            <div class="blog_post_title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></div>
            <div class="blog_post_info d-flex flex-row align-items-start justify-content-start">
                <div class="blog_post_author">By<a href="#"> <?php the_author() ?></a></div>
                <div class="blog_post_category">
                    <ul>
                        <li>in <?php the_category('<a></a>') ?></li>
                    </ul>
                </div>
                <div class="blog_post_comments"><a href="#"><?php if (!comments_open()) echo 'Comments are off for this post';
                                    else comments_number() ?></a></div>
            </div>
            <div class="blog_post_text">
                <p><?php the_content(); ?></p>
            </div>
        </div>
    </div>
</div>