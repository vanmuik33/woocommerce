<!-- <h1>Here is single.php</h1> -->
<?php
get_header('post');
?>
<div class="container">
	<div class="row">
		<div class="col-sm-8">
			<div class="" id="blog">
				<article id="<?php the_ID() ?>" <?php post_class('blog-post'); ?>>
					<div class="blog-post-meta">
						<?php $author_id = $post->post_author; ?>
						<span class="post-meta-author"> <i class="fa fa-user"></i><?php the_author_meta('user_nicename' , $author_id) ?></span>
						<span class="post-meta-time"> <i class="fa fa-calendar"></i>
							<time class="datetime" datetime="<?php echo get_the_date('Y-m-d g:i A') ?>"><?php echo get_the_date('F d, Y') ?></time>
						</span>
						<span class="post-meta-categories">
							<i class="fa fa-folder-o"></i>
							<?php the_category('<a></a>') ?>
						</span>
						<span class="post-meta-comments">
							<i class="fa fa-comment-o">
							</i>
							<?php if (!comments_open()) echo 'Comments are off for this post';
							else comments_number() ?>
						</span>
					</div>
					<div class="blog-post-entry">
						<?php the_excerpt() ?>
					</div>

					<div class="blog-post-author clearfix">						
						<?php echo get_avatar($author_id) ?>
						<h4><?php the_author_meta('user_nicename' , $author_id) ?></h4>
						<div><?php the_author_meta('user_description',$author_id) ?></div>
					</div>
					<div class="blog-post-related-articles">
						<div class="row">
							<div class="col-sm-12">
								<div class="related-article-title">Related Articles</div>
								<!--/.related-article-title-->
							</div>
							<!--/.col-sm-12-->
							<?php
							$tags = wp_get_post_tags(get_the_ID());
							if ($tags) $tags_ids = array();
							foreach ($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
							$args = array(
								'tag__in' => $tag_ids,
								'post__not_in' => array(get_the_ID()),
								'post_per_page' => 3
							);
							$my_query = new WP_Query($args);
							if ($my_query->have_posts()) :
								while ($my_query->have_posts()) : $my_query->the_post();
							?>
									<div class="col-sm-4">
										<a href="<?php the_permalink() ?>" title="<?php the_title() ?>" class="related-post" style="background-image: url(<?php the_post_thumbnail_url() ?>)">
											<span class="related-post-title"><?php the_title() ?></span>
										</a>
									</div>
							<?php
								endwhile;
								wp_reset_query();
							endif;
							?>
						</div>
						<!--/.row-->
					</div>
					<div id="comments">
						<?php // close to check single.php 
						if (comments_open() || get_comments_number()) :
							comments_template();
						endif;
						?>
					</div>
				</article>
			</div>
		</div>
		<div class="col-sm-4">
			<?php get_sidebar() ?>
		</div>
	</div>
</div>
<?php
get_footer();
?>