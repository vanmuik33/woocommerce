/* JS Document */

/******************************

[Table of Contents]

1. Vars and Inits
2. Init Menu


******************************/

$(document).ready(function () {
  "use strict";

  /* 

	1. Vars and Inits

	*/

  var menu = $(".menu");
  var burger = $(".hamburger");
  var menuActive = false;

  $(window).on("resize", function () {
    setTimeout(function () {
      $(window).trigger("resize.px.parallax");
    }, 375);
  });

  initMenu();

  /* 

	2. Init Menu

	*/

  function initMenu() {
    if (menu.length) {
      if ($(".hamburger").length) {
        burger.on("click", function () {
          if (menuActive) {
            closeMenu();
          } else {
            openMenu();

            $(document).one("click", function cls(e) {
              if ($(e.target).hasClass("menu_mm")) {
                $(document).one("click", cls);
              } else {
                closeMenu();
              }
            });
          }
        });
      }
    }
  }

  function openMenu() {
    menu.addClass("active");
    menuActive = true;
  }

  function closeMenu() {
    menu.removeClass("active");
    menuActive = false;
  }

  var currentPage = 1;
  $(".page-template-blog").on("click", " .load_more_button", function (e) {
    var self = $(this);
    e.preventDefault();
    $.ajax({
      // Hàm ajax
      type: "get", //Phương thức truyền post hoặc get
      dataType: "json", //Dạng dữ liệu trả về xml, json, script, or html
      url: my_ajaxurl, // Nơi xử lý dữ liệu
      data: {
        action: "load_more", //Tên action, dữ liệu gởi lên cho server
        currentPage: currentPage,
        post_per_page: 3,
      },
      beforeSend: function () {
        self.addClass("loading");
      },
      success: function (response) {
        self.removeClass("loading");
        var elem = $(response.data.html);
        console.log(response.data.next);
        $(".blog .blogs-content").append(elem);
        if ("false" == response.data.next) {
          $(".load_more_button").hide();
        }
        currentPage++;
      },
      error: function (jqXHR, textStatus, errorThrown) {
        //Làm gì đó khi có lỗi xảy ra
        console.log("The following error occured: " + textStatus, errorThrown);
      },
    });
  });
});
