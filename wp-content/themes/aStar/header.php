<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="aStar Fashion Template Project">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?php bloginfo('stylesheet_directory'); ?>/plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/plugins/OwlCarousel2-2.2.1/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/plugins/OwlCarousel2-2.2.1/animate.css">
    <?php wp_head(); ?>
</head>

<body <?php body_class() ?>>

    <div class="super_container">

        <!-- Header -->

        <header class="header">
            <div class="header_content d-flex flex-row align-items-center justify-content-start">

                <!-- Hamburger -->
                <div class="hamburger menu_mm"><i class="fa fa-bars menu_mm" aria-hidden="true"></i></div>

                <!-- Logo -->
                <div class="header_logo">
                    <a href="<?php bloginfo('url') ?>">
                        <div>a<span>star</span></div>
                    </a>
                </div>

                <!-- Navigation -->
                <nav class="header_nav">
                    <?php wp_nav_menu(
                        array(
                            'theme_location' => 'top-menu',
                            'container' => 'false',
                            'menu_id' => 'sidebar-menu',
                            'menu_class' => 'd-flex flex-row align-items-center justify-content-start'
                        )
                    ); ?>
                </nav>

                <!-- Header Extra -->
                <div class="header_extra ml-auto d-flex flex-row align-items-center justify-content-start">

                    <!-- Language -->
                    <div class="info_languages has_children">
                        <div class="language_flag"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/flag_1.svg" alt="https://www.flaticon.com/authors/freepik"></div>
                        <div class="dropdown_text">english</div>
                        <div class="dropdown_arrow"><i class="fa fa-angle-down" aria-hidden="true"></i></div>

                        <!-- Language Dropdown Menu -->
                        <ul>
                            <li><a href="#">
                                    <div class="language_flag"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/flag_2.svg" alt="https://www.flaticon.com/authors/freepik"></div>
                                    <div class="dropdown_text">french</div>
                                </a></li>
                            <li><a href="#">
                                    <div class="language_flag"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/flag_3.svg" alt="https://www.flaticon.com/authors/freepik"></div>
                                    <div class="dropdown_text">japanese</div>
                                </a></li>
                            <li><a href="#">
                                    <div class="language_flag"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/flag_4.svg" alt="https://www.flaticon.com/authors/freepik"></div>
                                    <div class="dropdown_text">russian</div>
                                </a></li>
                            <li><a href="#">
                                    <div class="language_flag"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/flag_5.svg" alt="https://www.flaticon.com/authors/freepik"></div>
                                    <div class="dropdown_text">spanish</div>
                                </a></li>
                        </ul>

                    </div>

                    <!-- Currency -->
                    <div class="info_currencies has_children">
                        <div class="dropdown_text">usd</div>
                        <div class="dropdown_arrow"><i class="fa fa-angle-down" aria-hidden="true"></i></div>

                        <!-- Currencies Dropdown Menu -->
                        <ul>
                            <li><a href="#">
                                    <div class="dropdown_text">EUR</div>
                                </a></li>
                            <li><a href="#">
                                    <div class="dropdown_text">YEN</div>
                                </a></li>
                            <li><a href="#">
                                    <div class="dropdown_text">GBP</div>
                                </a></li>
                            <li><a href="#">
                                    <div class="dropdown_text">CAD</div>
                                </a></li>
                        </ul>

                    </div>

                    <!-- Cart -->
                    <!-- The contents are written in inc/cart/cart.php file -->
                    <!-- Refer from page https://docs.woocommerce.com/document/show-cart-contents-total/-->
                    <div class="cart-customlocation">

                    </div>

                </div>

            </div>
        </header>