<?php

/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

global $product;
$product_tag = '';

if (is_new_product(6, $product)) $product_tag = 'new';
if ($product->is_featured()) $product_tag = 'hot';
if ($product->is_on_sale()) $product_tag = 'sale';
// Ensure visibility.
if (empty($product) || !$product->is_visible()) {
	return;
}
?>
<!-- Product -->
<div <?php wc_product_class('product grid-item ' . $product_tag, $product); ?>>
	<div class="product_inner">
		<div class="product_image">
			<img src="<?php echo get_the_post_thumbnail_url() ?>" alt="">
			<div class="product_tag"><?php echo $product_tag ?></div>
		</div>
		<div class="product_content text-center">
			<div class="product_title"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></div>
			<div class="product_price">
				<?php
				echo "$" . $product->get_price();
				if ($product->is_on_sale()) {
					echo "<span>RDP " . $product->get_regular_price() . "</span>";
				}
				?>
			</div>
			<div class="product_button ml-auto mr-auto trans_200"><?php woocommerce_template_loop_add_to_cart(); ?></div>
		</div>
	</div>
</div>

<?php
?>