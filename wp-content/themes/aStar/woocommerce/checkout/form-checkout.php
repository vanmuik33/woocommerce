<?php

/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

if (!defined('ABSPATH')) {
	exit;
}

do_action('woocommerce_before_checkout_form', $checkout);

// If checkout registration is disabled and not logged in, the user cannot checkout.
if (!$checkout->is_registration_enabled() && $checkout->is_registration_required() && !is_user_logged_in()) {
	echo esc_html(apply_filters('woocommerce_checkout_must_be_logged_in_message', __('You must be logged in to checkout.', 'woocommerce')));
	return;
}

?>
<div class="checkout">
	<div class="section_container">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="checkout_container d-flex flex-xxl-row flex-column align-items-start justify-content-start">
						<form name="checkout" method="post" class="checkout checkout_form woocommerce-checkout d-flex flex-xxl-row flex-column align-items-start" action="<?php echo esc_url(wc_get_checkout_url()); ?>" enctype="multipart/form-data">

							<!-- Billing -->
							<div class="billing checkout_box">
								<div class="checkout_title">Billing Address</div>
								<div class="checkout_form_container">

									<?php if ($checkout->get_checkout_fields()) : ?>

										<?php do_action('woocommerce_checkout_before_customer_details'); ?>

										<div class="col2-set" id="customer_details">
											<!-- <div class="col-1"> -->
											<?php do_action('woocommerce_checkout_billing'); ?>
											<!-- </div> -->

											<!-- <div class="col-2"> -->
												<!-- <?php do_action('woocommerce_checkout_shipping'); ?> -->
											<!-- </div> -->
										</div>

										<?php do_action('woocommerce_checkout_after_customer_details'); ?>

									<?php endif; ?>

									<div class="checkout_extra">
										<ul>
											<li class="billing_info d-flex flex-row align-items-center justify-content-start">
												<label class="checkbox_container">
													<?php if (wc_terms_and_conditions_checkbox_enabled()) : ?>
														<input type="checkbox" id="cb_1" name="terms" class="billing_checkbox">
														<input type="hidden" name="terms-field" value="1"
														<?php checked(apply_filters('woocommerce_terms_is_checked_default', isset($_POST['terms'])), true); // WPCS: input var ok, csrf ok. 
														?> id="terms" />
													<?php endif; ?>
													<span class="checkbox_mark"></span>
													<span class="checkbox_text">Terms and conditions</span>
												</label>
											</li>
											<li class="billing_info d-flex flex-row align-items-center justify-content-start">
												<label class="checkbox_container">
													<input type="checkbox" id="cb_2" name="billing_checkbox" class="billing_checkbox">
													<span class="checkbox_mark"></span>
													<span class="checkbox_text">Create an account</span>
												</label>
											</li>
											<li class="billing_info d-flex flex-row align-items-center justify-content-start">
												<label class="checkbox_container">
													<input type="checkbox" id="cb_3" name="billing_checkbox" class="billing_checkbox">
													<span class="checkbox_mark"></span>
													<span class="checkbox_text">Subscribe to our newsletter</span>
												</label>
											</li>
										</ul>
									</div>

								</div>
							</div>
							<!-- Cart Total -->
							<div class="cart_total">
								<div class="cart_total_inner checkout_box">
									<div class="checkout_title">Cart total</div>
									<ul class="cart_extra_total_list">
										<li class="d-flex flex-row align-items-center justify-content-start">
											<div class="cart_extra_total_title">Subtotal</div>
											<div data-title="<?php esc_attr_e('Subtotal', 'woocommerce'); ?>" class="cart_extra_total_value ml-auto"><?php wc_cart_totals_subtotal_html(); ?></div>
										</li>
										<li class="d-flex flex-row align-items-center justify-content-start">
											<div class="cart_extra_total_title" style="width:100%">Coupon
												<ul class="coupon-items">
													<?php foreach (WC()->cart->get_coupons() as $code => $coupon) : ?>
														<li class="coupon-item d-flex">
															<span><?php my_wc_cart_totals_coupon_label($coupon); ?></span>
															<span class="ml-auto" data-title="<?php echo esc_attr(wc_cart_totals_coupon_label($coupon, false)); ?>"><?php wc_cart_totals_coupon_html($coupon); ?></span>
														</li>
													<?php endforeach; ?>
												</ul>
											</div>
										</li>
										<li class="d-flex flex-row align-items-center justify-content-start">
											<div class="cart_extra_total_title">Shipping</div>
											<div class="cart_extra_total_value ml-auto">
												<?php
												$current_shipping_cost = WC()->cart->get_cart_shipping_total();
												echo $current_shipping_cost;
												?>
											</div>
										</li>
										<li class="d-flex flex-row align-items-center justify-content-start">
											<div class="cart_extra_total_title">Total</div>
											<div data-title="<?php esc_attr_e('Total', 'woocommerce'); ?>" class="cart_extra_total_value ml-auto"><?php wc_cart_totals_order_total_html(); ?></div>
										</li>
									</ul>

									<!-- Payment Options -->
									<div class="payment">
										<div class="payment_options">
											<?php do_action('woocommerce_checkout_before_order_review_heading'); ?>

											<!-- <h3 id="order_review_heading"><?php esc_html_e('Your order', 'woocommerce'); ?></h3> -->

											<?php do_action('woocommerce_checkout_before_order_review'); ?>

											<div id="order_review" class="woocommerce-checkout-review-order">
												<?php do_action('woocommerce_checkout_order_review'); ?>
											</div>

											<?php do_action('woocommerce_checkout_after_order_review'); ?>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php do_action('woocommerce_after_checkout_form', $checkout); ?>