<?php

/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.8.0
 */

defined('ABSPATH') || exit;

do_action('woocommerce_before_cart'); ?>

<form class="woocommerce-cart-form" id="cart-form" action="<?php echo esc_url(wc_get_cart_url()); ?>" method="post">
	<div class="cart_section">
		<div class="section_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="cart_container">
							<!-- Cart Bar -->
							<div class="cart_bar">
								<ul class="cart_bar_list item_list d-flex flex-row align-items-center justify-content-start">
									<li>Product</li>
									<li>Color</li>
									<li>Size</li>
									<li>Price</li>
									<li>Quantity</li>
									<li>Total</li>
								</ul>
							</div>
							<?php do_action('woocommerce_before_cart_table'); ?>
							<!-- Cart Items -->
							<div class="cart_items">
								<ul class="cart_items_list">
									<?php
									foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
										$_product   = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
										$product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);
										$product = wc_get_product($product_id);
										if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
											$product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key);
									?>
											<!-- Cart Item -->
											<li class="cart_item item_list d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-start">
												<div class="product d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-start">
													<div>
														<div class="product_image">
															<img src="<?php echo get_the_post_thumbnail_url($product_id) ?>" alt="">
														</div>
													</div>
													<div class="product_name"><a href="<?php echo $product_permalink; ?>"><?php echo $product->get_name() ?></a></div>
												</div>
												<div class="product_color text-lg-center product_text"><span>Color: </span>
													<?php
													echo ($_product->get_attribute('pa_color'));
													?>
												</div>
												<div class="product_size text-lg-center product_text"><span>Size: </span>
													<?php
													if ($product->is_type('simple')) {
														echo 'One size';
													};
													if ($product->is_type('variable')) {
														echo $cart_item['variation']['attribute_pa_size'];
													};												?>
												</div>
												<div class="product_price text-lg-center product_text"><span>Price: </span><?php echo "$" . $_product->get_price() ?></div>
												<div class="product_quantity_container">
													<div class="product_quantity ml-lg-auto mr-lg-auto text-center">
														<?php
														if ($_product->is_sold_individually()) {
															$product_quantity = sprintf('1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key);
														} else {
															$product_quantity = woocommerce_quantity_input(
																array(
																	'input_name'   => "cart[{$cart_item_key}][qty]",
																	'input_value'  => $cart_item['quantity'],
																	'max_value'    => $_product->get_max_purchase_quantity(),
																	'min_value'    => '0',
																	'product_name' => $_product->get_name(),
																),
																$_product,
																false
															);
														}
														echo apply_filters('woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item); // PHPCS: XSS ok.
														?>
														<span class="product_text product_num"><?php echo $cart_item['quantity']; ?></span>
														<div class="qty_sub qty_button trans_200 text-center"><span>-</span></div>
														<div class="qty_add qty_button trans_200 text-center"><span>+</span></div>
													</div>

												</div>
												<div class="product_total text-lg-center product_text"><span>Total: </span><?php echo "$" . (WC()->cart->get_product_subtotal($_product, $cart_item['quantity'])) ?></div>
											</li>
									<?php
										}
									}
									?>
								</ul>
							</div>
							<!-- Cart Buttons -->
							<div class="cart_buttons d-flex flex-row align-items-start justify-content-start">
								<div class="cart_buttons_inner ml-auto d-flex flex-row align-items-start justify-content-start flex-wrap">
									<div class="button button_continue trans_200"><a href="<?php echo ($shop_page_url = get_permalink(wc_get_page_id('shop'))); ?>">continue shopping</a></div>
									<div class="button button_clear trans_200"><a href="<?php echo esc_url(add_query_arg('empty_cart', 'yes')) ?>">clear cart</a></div>
									<button type="submit" class="button button_update trans_200" name="update_cart" value="<?php esc_attr_e('Update cart', 'woocommerce'); ?>"><?php esc_html_e('Update cart', 'woocommerce'); ?></button>
									<?php do_action('woocommerce_cart_actions'); ?>
									<?php wp_nonce_field('woocommerce-cart', 'woocommerce-cart-nonce'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>







		<div class="section_container cart_extra_container">
			<div class="container">
				<div class="row">
					<!-- Shipping & Delivery -->
					<div class="col-xxl-6">
						<div class="cart_extra cart_extra_1">
							<div class="cart_extra_content cart_extra_coupon">
								<div class="cart_extra_title">Coupon code</div>
								<div class="coupon_form_container">
									<form action="#" id="coupon_form" class="coupon_form">
										<?php if (wc_coupons_enabled()) { ?>
											<div class="coupon">
												<input type="text" name="coupon_code" class="input-text coupon_input" id="coupon_code" value="" /> <button type="submit" class="coupon_button" name="apply_coupon" value="<?php esc_attr_e('Apply code', 'woocommerce'); ?>"><?php esc_attr_e('APPLY CODE', 'woocommerce'); ?></button>
												<?php do_action('woocommerce_cart_coupon'); ?>
											</div>
										<?php } ?>
									</form>
								</div>
								<div class="shipping">
									<div class="cart_extra_title">Shipping Method</div>
									<?php do_action('my_woocommerce_cart_collaterals'); ?>
								</div>
							</div>
						</div>
					</div>

					<!-- Cart Total -->
					<div class="col-xxl-6">
						<div class="cart_extra cart_extra_2">
							<div class="cart_extra_content cart_extra_total">
								<div class="cart_extra_title">Cart Total</div>
								<ul class="cart_extra_total_list">
									<?php do_action('woocommerce_cart_collaterals'); ?>
								</ul>
								<div class="checkout_button trans_200"><?php do_action('woocommerce_proceed_to_checkout'); ?></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</form>