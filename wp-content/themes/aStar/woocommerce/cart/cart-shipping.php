<?php

/**
 * Shipping Methods Display
 *
 * In 2.1 we show methods per package. This allows for multiple methods per order if so desired.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-shipping.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;
if ($available_methods) : ?>
	<ul id="shipping_method" class="woocommerce-shipping-methods">
		<?php foreach ($available_methods as $method) : ?>
			<li class="shipping_option d-flex flex-row align-items-center justify-content-start">
				<label class="radio_container">
					<?php
					if (1 < count($available_methods)) {
						printf('<input type="radio" name="shipping_method[%1$d]" data-index="%1$d" id="shipping_method_%1$d_%2$s" value="%3$s" class="shipping_method shipping_radio" %4$s /><span class="radio_mark"></span>', $index, esc_attr(sanitize_title($method->id)), esc_attr($method->id), checked($method->id, $chosen_method, false)); // WPCS: XSS ok.
					} else {
						printf('<input type="hidden" name="shipping_method[%1$d]" data-index="%1$d" id="shipping_method_%1$d_%2$s" value="%3$s" class="shipping_method" />', $index, esc_attr(sanitize_title($method->id)), esc_attr($method->id)); // WPCS: XSS ok.
					}
					// printf('<span for="shipping_method_%1$s_%2$s">%3$s</span>', $index, esc_attr(sanitize_title($method->id)), wc_cart_totals_shipping_method_label($method)); // WPCS: XSS ok.
					printf('<span for="shipping_method_%1$s_%2$s">%3$s</span>', $index, esc_attr(sanitize_title($method->id)), $method->get_label()); // WPCS: XSS ok.
					do_action('woocommerce_after_shipping_rate', $method, $index);
					?>
				</label>
				<!-- <div class="shipping_price ml-auto"><?php echo '$' . $method->cost; ?></div> -->
				<?php printf('<div class="shipping_price ml-auto">$%1s</div>', $method->cost); ?>

			</li>
		<?php endforeach; ?>
	</ul>
	<?php if (is_cart()) : ?>
		<!-- <p class="woocommerce-shipping-destination">
			<?php
			if ($formatted_destination) {
				// Translators: $s shipping destination.
				printf(esc_html__('Shipping to %s.', 'woocommerce') . ' ', '<strong>' . esc_html($formatted_destination) . '</strong>');
				$calculator_text = esc_html__('Change address', 'woocommerce');
			} else {
				echo wp_kses_post(apply_filters('woocommerce_shipping_estimate_html', __('Shipping options will be updated during checkout.', 'woocommerce')));
			}
			?>
		</p> -->
	<?php endif; ?>
<?php endif; ?>