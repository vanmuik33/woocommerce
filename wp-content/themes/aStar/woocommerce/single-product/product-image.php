<?php

/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.1
 */

defined('ABSPATH') || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if (!function_exists('wc_get_gallery_image_html')) {
	return;
}

global $product;
$attachment_ids = $product->get_gallery_image_ids();
foreach ($attachment_ids as $attachment_id) {
	$gallery[] = wp_get_attachment_url($attachment_id);
}
$product_image =  wp_get_attachment_url( $product->get_image_id() )
?>

<div class="product_image_row d-flex flex-md-row flex-column align-items-md-end align-items-start justify-content-start">
	<div class="product_image_1 product_image">
		<img src="<?php echo $gallery[0]  ?>" alt="">
	</div>
	<div class="product_image_2 product_image"><img src="<?php echo $gallery[1] ?>" alt=""></div>
</div>
<div class="product_image_row">
	<div class="product_image_3 product_image"><img src="<?php echo $gallery[2] ?>" alt=""></div>
</div>
<div class="product_image_row d-flex flex-md-row flex-column align-items-start justify-content-start">
	<div class="product_image_4 product_image"><img src="<?php echo $gallery[3] ?>" alt=""></div>
	<div class="product_image_5 product_image"><img src="<?php echo $gallery[4] ?>" alt=""></div>
</div>