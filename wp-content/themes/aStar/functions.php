<?php
@ini_set( 'upload_max_size' , '120M' );
@ini_set( 'post_max_size', '120M');
@ini_set( 'max_execution_time', '300' );

define('TPL', 'template-parts/');
define('TPL_DIR_URI', get_template_directory_uri());

function my_custom_wc_theme_support()
{
    add_theme_support('woocommerce');
    add_theme_support('wc-product-gallery-lightbox');
    add_theme_support('wc-product-gallery-slider');
    add_theme_support('title-tag');
}
add_action('after_setup_theme', 'my_custom_wc_theme_support');






function add_theme_scripts()
{
    wp_enqueue_style('style', get_stylesheet_uri());
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/styles/bootstrap-4.1.3/bootstrap.css', array(), '1.1', 'all');
    if (is_home() || is_front_page()) {
        wp_enqueue_style('main-style', get_template_directory_uri() . '/styles/main_styles.css', array(), '1.1', 'all');
        wp_enqueue_style('main-style-responsive', get_template_directory_uri() . '/styles/responsive.css', array(), '1.1', 'all');
    }
    if (is_product()) {
        wp_enqueue_style('product', get_template_directory_uri() . '/styles/product.css', array(), '1.1', 'all');
        wp_enqueue_style('product-responsive', get_template_directory_uri() . '/styles/product_responsive.css', array(), '1.1', 'all');
    }
    if (is_archive()) {
        wp_enqueue_style('categories', get_template_directory_uri() . '/styles/categories.css', array(), '1.1', 'all');
        wp_enqueue_style('categories-responsive', get_template_directory_uri() . '/styles/categories_responsive.css', array(), '1.1', 'all');
    }
    if (is_page('blog')) {
        wp_enqueue_style('blog', get_template_directory_uri() . '/styles/blog.css', array(), '1.1', 'all');
        wp_enqueue_style('blog-responsive', get_template_directory_uri() . '/styles/blog_responsive.css', array(), '1.1', 'all');
    }
    if (is_cart()) {
        wp_enqueue_style('cart', get_template_directory_uri() . '/styles/cart.css', array(), '1.1', 'all');
        wp_enqueue_style('cart-responsive', get_template_directory_uri() . '/styles/cart_responsive.css', array(), '1.1', 'all');
    }
    if (is_checkout()) {
        wp_enqueue_style('checkout', get_template_directory_uri() . '/styles/checkout.css', array(), '1.1', 'all');
        wp_enqueue_style('checkout-responsive', get_template_directory_uri() . '/styles/checkout_responsive.css', array(), '1.1', 'all');
    }

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}
add_action('wp_enqueue_scripts', 'add_theme_scripts');

function initTheme()
{
    // Register menu
    register_nav_menu('sidebar-menu', __('Sidebar Menu'));
    register_nav_menu('top-menu', __('Top Main Menu'));
}
add_action('init', 'initTheme');


/* Disable woocommerce style
*
*
*/
add_filter('woocommerce_enqueue_styles', '__return_empty_array');

require_once 'inc/inc.php';
require_once 'inc/ajax/blog-ajax.php';