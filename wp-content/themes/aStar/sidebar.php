<!-- Sidebar -->

<div class="sidebar">

    <!-- Info -->
    <div class="info">
        <div class="info_content d-flex flex-row align-items-center justify-content-start">

            <!-- Language -->
            <div class="info_languages has_children">
                <div class="language_flag"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/flag_1.svg" alt="https://www.flaticon.com/authors/freepik"></div>
                <div class="dropdown_text">english</div>
                <div class="dropdown_arrow"><i class="fa fa-angle-down" aria-hidden="true"></i></div>

                <!-- Language Dropdown Menu -->
                <ul>
                    <li><a href="#">
                            <div class="language_flag"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/flag_2.svg" alt="https://www.flaticon.com/authors/freepik"></div>
                            <div class="dropdown_text">french</div>
                        </a></li>
                    <li><a href="#">
                            <div class="language_flag"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/flag_3.svg" alt="https://www.flaticon.com/authors/freepik"></div>
                            <div class="dropdown_text">japanese</div>
                        </a></li>
                    <li><a href="#">
                            <div class="language_flag"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/flag_4.svg" alt="https://www.flaticon.com/authors/freepik"></div>
                            <div class="dropdown_text">russian</div>
                        </a></li>
                    <li><a href="#">
                            <div class="language_flag"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/flag_5.svg" alt="https://www.flaticon.com/authors/freepik"></div>
                            <div class="dropdown_text">spanish</div>
                        </a></li>
                </ul>

            </div>

            <!-- Currency -->
            <div class="info_currencies has_children">
                <div class="dropdown_text">usd</div>
                <div class="dropdown_arrow"><i class="fa fa-angle-down" aria-hidden="true"></i></div>

                <!-- Currencies Dropdown Menu -->
                <ul>
                    <li><a href="#">
                            <div class="dropdown_text">EUR</div>
                        </a></li>
                    <li><a href="#">
                            <div class="dropdown_text">YEN</div>
                        </a></li>
                    <li><a href="#">
                            <div class="dropdown_text">GBP</div>
                        </a></li>
                    <li><a href="#">
                            <div class="dropdown_text">CAD</div>
                        </a></li>
                </ul>

            </div>

        </div>
    </div>

    <!-- Logo -->
    <div class="sidebar_logo">
        <a href="<?php bloginfo('url') ?>">
            <div>a<span>star</span></div>
        </a>
    </div>

    <!-- Sidebar Navigation -->
    <nav class="sidebar_nav">
        <?php wp_nav_menu(
            array(
                'theme_location' => 'sidebar-menu',
                'container' => 'false',
                'menu_id' => 'sidebar-menu',
                'menu_class' => 'sidebar-menu'
            )
        ); ?>
    </nav>

    <!-- Search -->
    <div class="search">
        <form action="#" class="search_form" id="sidebar_search_form">
            <input type="text" class="search_input" placeholder="Search" required="required">
            <button class="search_button"><i class="fa fa-search" aria-hidden="true"></i></button>
        </form>
    </div>

    <!-- Cart -->
    <!-- The contents are written in inc/cart/cart.php file -->
    <!-- Refer from page https://docs.woocommerce.com/document/show-cart-contents-total/-->
    <div class="cart-customlocation">

    </div>
</div>