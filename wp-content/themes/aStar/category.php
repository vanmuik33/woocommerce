<?php get_header('post'); ?>
<div class="container">
    <div class="row">
        <div class="col-sm-8">
            <section id="news">
                <?php
                if (have_posts()) :
                    while (have_posts()) :
                        the_post();
                ?>
                        <article id="<?php the_ID() ?>" <?php post_class(); ?>>
                            <a href="<?php the_permalink() ?>" title="<?php the_title() ?>" class="news-title"><?php the_title() ?></a>
                            <div class="news-image">
                                <a href="<?php the_permalink() ?>"><?php the_post_thumbnail() ?></a>
                            </div>
                            <div class="news-post-meta">
                                <span class="post-meta-author"> <i class="fa fa-user"><?php the_author() ?> </i></span>
                                <span class="post-meta-time"> <i class="fa fa-calendar"></i>
                                    <time class="datetime" datetime="<?php echo get_the_date('Y-m-d g:i A') ?>"><?php echo get_the_date('F d, Y') ?></time>
                                </span>
                                <span class="post-meta-categories">
                                    <i class="fa fa-folder-o"></i>
                                </span>
                            </div>
                            
                        </article>
                <?php
                    endwhile;
                endif;
                ?>
            </section>
        </div>
    </div>
</div>
<?php get_footer(); ?>