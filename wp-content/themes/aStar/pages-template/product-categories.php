<?php
/*
    Template Name: Product Categories
*/
get_header();
get_template_part(TPL.'home');
echo do_shortcode( '[product_categories]');
get_footer();