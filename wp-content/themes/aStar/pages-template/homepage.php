<?php
/*
Template Name: Homepage */
get_header();
?>

<!-- Menu -->
<div class="menu d-flex flex-column align-items-start justify-content-start menu_mm trans_400">
    <div class="menu_close_container">
        <div class="menu_close">
            <div></div>
            <div></div>
        </div>
    </div>
    <div class="menu_top d-flex flex-row align-items-center justify-content-start">

        <!-- Language -->
        <div class="info_languages has_children">
            <div class="language_flag"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/flag_1.svg" alt="https://www.flaticon.com/authors/freepik"></div>
            <div class="dropdown_text">english</div>
            <div class="dropdown_arrow"><i class="fa fa-angle-down" aria-hidden="true"></i></div>

            <!-- Language Dropdown Menu -->
            <ul>
                <li><a href="#">
                        <div class="language_flag"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/flag_2.svg" alt="https://www.flaticon.com/authors/freepik"></div>
                        <div class="dropdown_text">french</div>
                    </a></li>
                <li><a href="#">
                        <div class="language_flag"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/flag_3.svg" alt="https://www.flaticon.com/authors/freepik"></div>
                        <div class="dropdown_text">japanese</div>
                    </a></li>
                <li><a href="#">
                        <div class="language_flag"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/flag_4.svg" alt="https://www.flaticon.com/authors/freepik"></div>
                        <div class="dropdown_text">russian</div>
                    </a></li>
                <li><a href="#">
                        <div class="language_flag"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/flag_5.svg" alt="https://www.flaticon.com/authors/freepik"></div>
                        <div class="dropdown_text">spanish</div>
                    </a></li>
            </ul>

        </div>

        <!-- Currency -->
        <div class="info_currencies has_children">
            <div class="dropdown_text">usd</div>
            <div class="dropdown_arrow"><i class="fa fa-angle-down" aria-hidden="true"></i></div>

            <!-- Currencies Dropdown Menu -->
            <ul>
                <li><a href="#">
                        <div class="dropdown_text">EUR</div>
                    </a></li>
                <li><a href="#">
                        <div class="dropdown_text">YEN</div>
                    </a></li>
                <li><a href="#">
                        <div class="dropdown_text">GBP</div>
                    </a></li>
                <li><a href="#">
                        <div class="dropdown_text">CAD</div>
                    </a></li>
            </ul>

        </div>

    </div>
    <div class="menu_search">
        <form action="#" class="header_search_form menu_mm">
            <input type="search" class="search_input menu_mm" placeholder="Search" required="required">
            <button class="header_search_button d-flex flex-column align-items-center justify-content-center menu_mm">
                <i class="fa fa-search menu_mm" aria-hidden="true"></i>
            </button>
        </form>
    </div>
    <nav class="menu_nav">
        <ul class="menu_mm">
            <li class="menu_mm"><a href="index.html">home</a></li>
            <li class="menu_mm"><a href="#">woman</a></li>
            <li class="menu_mm"><a href="#">man</a></li>
            <li class="menu_mm"><a href="#">lookbook</a></li>
            <li class="menu_mm"><a href="blog.html">blog</a></li>
            <li class="menu_mm"><a href="contact.html">contact</a></li>
        </ul>
    </nav>
    <div class="menu_extra">
        <div class="menu_social">
            <ul>
                <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            </ul>
        </div>
    </div>
</div>

<?php get_sidebar() ?>
<?php get_template_part(TPL.'home') ?>
<!-- Boxes -->

<div class="boxes">
    <div class="section_container">
        <div class="container">
            <div class="row">

                <!-- Box -->
                <div class="col-lg-4 box_col">
                    <div class="box">
                        <div class="box_image"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/box_1.jpg" alt=""></div>
                        <div class="box_title trans_200"><a href="categories.html">summer collection</a></div>
                    </div>
                </div>

                <!-- Box -->
                <div class="col-lg-4 box_col">
                    <div class="box">
                        <div class="box_image"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/box_2.jpg" alt=""></div>
                        <div class="box_title trans_200"><a href="categories.html">eyewear collection</a></div>
                    </div>
                </div>

                <!-- Box -->
                <div class="col-lg-4 box_col">
                    <div class="box">
                        <div class="box_image"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/box_3.jpg" alt=""></div>
                        <div class="box_title trans_200"><a href="categories.html">basic pieces</a></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Categories -->

<div class="categories">
    <div class="section_container">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <div class="categories_list_container">
                        <ul class="categories_list d-flex flex-row align-items-center justify-content-start">
                            <li><a href="#>" data-cat="new">new arrivals</a></li>
                            <li><a href="#" data-cat="sale">recommended</a></li>
                            <li><a href="#" data-cat="hot">best sellers</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Products -->

<div class="products">
    <div class="section_container">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="products_container grid">
                        <?php $args = array('post_type' => 'product', 'posts_per_page' => 8, 'ignore_sticky_posts' => 1); ?>
                        <?php $getposts = new WP_query($args); ?>
                        <?php global $wp_query;
                        $wp_query->in_the_loop = true; ?>
                        <?php while ($getposts->have_posts()) : $getposts->the_post();
                            get_template_part('template-parts/content/content', 'product');
                        endwhile;
                        wp_reset_postdata();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_template_part(TPL.'new_letter') ?>

<?php get_footer() ?>