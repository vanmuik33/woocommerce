<?php /* Template Name: Blog */ ?>
<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 * 
 */

get_header();
get_template_part(TPL . 'home');
?>
<?php
$args = array(
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => 3,
    'paged' => get_query_var('paged')
);
$loop = new WP_Query($args);

?>
<main id="site-content" role="main">

    <?php

    if ($loop->have_posts()) { ?>
        <!-- Blog -->

        <div class="blog">
            <div class="section_container">
                <div class="container">
                    <div class="row">
                        <div class="col blogs-content">
                            <?php
                            while ($loop->have_posts()) {
                                $loop->the_post();
                                get_template_part('template-parts/content', 'blog');
                            }
                            ?>
                        </div>
                    </div>
                    <div class="row load_more_row">
                        <div class="col">
                            <div class="load_more_button trans_200 ml-auto mr-auto"><a href="#">load more</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php
    }
    ?>


</main><!-- #site-content -->
<?php get_sidebar(); ?>
<?php get_template_part('template-parts/footer-menus-widgets'); ?>
<?php get_footer(); ?>