<?php

/*
Plugin Name: Newsletters - WooCommerce Subscribers
Plugin URI: https://tribulant.com/extensions/view/42/woocommerce-subscribers
Description: Capture email subscribers from WooCommerce plugin to the Newsletter plugin
Author: Tribulant
Version: 1.4
Author URI: https://tribulant.com
Text Domain: newsletters-woocommerce
Domain Path: /languages
*/

if (!defined('DS')) { define('DS', DIRECTORY_SEPARATOR); }
if (!defined('NEWSLETTERS_EXTENSION_URL')) { define('NEWSLETTERS_EXTENSION_URL', "http://tribulant.com/extensions/"); }

if (!class_exists('newsletters_woocommerce')) {
	$path = dirname(dirname(__FILE__)) . DS . 'wp-mailinglist' . DS . 'wp-mailinglist.php';
	$path2 = dirname(dirname(__FILE__)) . DS . 'newsletters-lite' . DS . 'wp-mailinglist.php';
	
	if (file_exists($path)) {
		require_once($path);
	} elseif (file_exists($path2)) {
		require_once($path2);
	} elseif (defined('NEWSLETTERS_NAME')) {
		$path = dirname(dirname(__FILE__)) . DS . NEWSLETTERS_NAME . DS . 'wp-mailinglist.php';
		if (file_exists($path)) {
			require_once($path);
		}
	}
	
	if (class_exists('wpMailPlugin')) {
		class newsletters_woocommerce extends wpMailPlugin {
			
			function __construct() {
				$this -> sections = (object) $this -> sections;
				$this -> extension_name = basename(dirname(__FILE__));
				$this -> extension_path = plugin_basename(__FILE__);
				$this -> extension_version = '1.4';
				
				$path1 = dirname(dirname(__FILE__)) . DS . 'wp-mailinglist' . DS . 'wp-mailinglist.php';
				$path2 = dirname(dirname(__FILE__)) . DS . 'newsletters-lite' . DS . 'wp-mailinglist.php';
				
				if (file_exists($path1)) {
					$this -> parent_path = plugin_basename('wp-mailinglist' . DS . 'wp-mailinglist.php');
				} elseif (file_exists($path2)) {
					$this -> parent_path = plugin_basename('newsletters-lite' . DS . 'wp-mailinglist.php');
				} elseif (defined('NEWSLETTERS_NAME') && file_exists(dirname(dirname(__FILE__)) . DS . NEWSLETTERS_NAME . DS . 'wp-mailinglist.php')) {
					$this -> parent_path = plugin_basename(NEWSLETTERS_NAME . DS . 'wp-mailinglist.php');
				}
				
				// Determine the checkbox hook
				$newsletters_woocommerce_checkbox_action = $this -> get_option('newsletters_woocommerce_checkbox_action');
				if (!empty($newsletters_woocommerce_checkbox_action)) {
					add_action($newsletters_woocommerce_checkbox_action, array($this, 'apply_newsletters_checkbox'), 10, 1);	
				}
			}
			
			function activation_hook() {
				require_once ABSPATH . 'wp-admin' . DS . 'includes' . DS . 'admin.php';
				$path = 'woocommerce' . DS . 'woocommerce.php';
				
				if (!is_plugin_active(plugin_basename($path)) || 
					!is_plugin_active(plugin_basename($this -> parent_path))) {
					_e('You must have the Newsletter plugin and WooCommerce plugin installed and activated in order to use this.', 'newsletters-woocommerce');
					exit(); die();
				} else {		
					$plugin_data = get_plugin_data(WP_PLUGIN_DIR . DS . $path);
					$plugin_version = $plugin_data['Version'];
					$plugin_data2 = get_plugin_data(WP_PLUGIN_DIR . DS . $this -> parent_path);
					$plugin_version2 = $plugin_data2['Version'];
					$woocommerce_required = '2.1';
					$newsletters_required = '4.1';
					
					$versiongood = false;
					if (version_compare($plugin_version, $checkout_required) >= 0 && 
						version_compare($plugin_version2, $newsletters_required) >= 0) {
						$versiongood = true;	
					}
					
					if ($versiongood == true) {
						flush_rewrite_rules();
						$this -> add_option('newsletters_woocommerce_checkbox', 1);
						$this -> add_option('newsletters_woocommerce_checkboxautocheck', 1);
						$this -> add_option('newsletters_woocommerce_checkboxlabel', __('Subscribe me to your newsletter'));
						$this -> add_option('newsletters_woocommerce_liststype', "admin");
						$this -> add_option('newsletters_woocommerce_liststype_user_selection', "multiple");
						$this -> add_option('newsletters_woocommerce_checkbox_action', 'woocommerce_after_checkout_billing_form');
					} else {
						echo sprintf(__('The %s extension requires the WooCommerce plugin %s and Newsletter plugin %s at least.', 'newsletters-woocommerce'), 'WooCommerce Subscribers', $woocommerce_required, $newsletters_required);
						exit(); die();	
					}
				}
				
				return true;
			}
			
			function init_textdomain() {
				if (function_exists('load_plugin_textdomain')) {			
					load_plugin_textdomain('newsletters-woocommerce', false, $this -> extension_name . DS . 'languages' . DS);
				}
			}
			
			function after_plugin_row($plugin_name = null) {		
		        $key = $this -> get_option('serialkey');
		        $update = $this -> vendor('update');
		        $version_info = $update -> get_version_info();
		    }
			
			function display_changelog() { 				
				if (!empty($_GET['plugin']) && $_GET['plugin'] == $this -> extension_name) { 		 
			    	require_once dirname(__FILE__) . DS . 'vendors' . DS . 'class.update.php';
					$update = new newsletters_woocommerce_update();
			    	$changelog = $update -> get_changelog();			    	
			    	//$this -> render('views' . DS . 'changelog', array('changelog' => $changelog), true, false, 'newsletters_woocommerce');			    	
			    	echo $changelog;
			    	exit();
			    }
		    }
			
			function has_update($cache = true) {
				require_once dirname(__FILE__) . DS . 'vendors' . DS . 'class.update.php';
				$update = new newsletters_woocommerce_update();
		        $version_info = $update -> get_version_info($cache);
		        return version_compare($this -> extension_version, $version_info["version"], '<');
		    }
			
			function check_update($option, $cache = true) {
				require_once dirname(__FILE__) . DS . 'vendors' . DS . 'class.update.php';
				$update = new newsletters_woocommerce_update();
		        $version_info = $update -> get_version_info($cache);
		
		        if (!$version_info) { return $option; }
		
		        $plugin_path = $this -> extension_name . '/woocommerce.php';
		        
		        if(empty($option -> response[$plugin_path])) {
					$option -> response[$plugin_path] = new stdClass();
		        }
		
		        //Empty response means that the key is invalid. Do not queue for upgrade
		        if(!$version_info["is_valid_key"] || version_compare($this -> extension_version, $version_info["version"], '>=')){
		            unset($option -> response[$plugin_path]);
		        } else {
		            $option -> response[$plugin_path] -> url = "http://tribulant.com";
		            $option -> response[$plugin_path] -> slug = $this -> extension_name;
		            $option -> response[$plugin_path] -> package = $version_info['url'];
		            $option -> response[$plugin_path] -> new_version = $version_info["version"];
		            $option -> response[$plugin_path] -> id = "0";
		        }
		
		        return $option;
		    }
			
			function metaboxes_extensions_settings($page = null) {
				add_meta_box('newsletters_woocommerce', '<img src="' . plugins_url() . '/' . $this -> extension_name . '/images/icon-16.png" /> ' . __('WooCommerce Subscribers', 'newsletters-woocommerce'), array($this, 'extensions_settings'), $page, 'normal', 'core');
			}
			
			function extensions_settings() {
				$this -> render('views' . DS . 'settings', false, true, false, 'newsletters_woocommerce');
			}
			
			function extensions_list($extensions = array()) {
				$extensions['newsletters_woocommerce'] = array(
					'name'			=>	__('WooCommerce Subscribers', 'newsletters-woocommerce'),
					'link'			=>	"https://tribulant.com/extensions/view/42/woocommerce-subscribers",
					'image'			=>	plugins_url() . '/' . $this -> extension_name . '/images/icon.png',
					'description'	=>	sprintf(__("Capture email subscribers from the WooCommerce plugin to the %sNewsletter plugin%s.", 'newsletters-woocommerce'), '<a href="https://tribulant.com/plugins/view/1/wordpress-newsletter-plugin" target="_blank">', '</a>'),
					'slug'			=>	'newsletters_woocommerce',
					'plugin_name'	=>	$this -> extension_name,
					'plugin_file'	=>	'woocommerce.php',
					'settings'		=>	admin_url('admin.php?page=' . $this -> sections -> extensions_settings . '#newsletters_woocommerce'),
				);
				
				$titles = array();
				foreach ($extensions as $extension) {
					$titles[] = $extension['name'];
				}
				
				array_multisort($titles, SORT_ASC, $extensions);
				return $extensions;
			}
			
			function newsletters_extensions_settings_saved($post = null) {
				
				if ($this -> language_do()) {
					foreach ($_POST as $pkey => $pval) {
						switch ($pkey) {
							case 'newsletters_woocommerce_checkboxlabel'				:
								$_POST[$pkey] = $this -> language_join($_POST[$pkey]);
								$this -> update_option($pkey, $_POST[$pkey]);
								break;
						}
					}
				}
				
				if (empty($_POST['newsletters_woocommerce_checkbox'])) {
					$this -> delete_option('newsletters_woocommerce_checkbox');
				}
				
				if (empty($_POST['newsletters_woocommerce_checkboxautocheck'])) {
					$this -> delete_option('newsletters_woocommerce_checkboxautocheck');
				}
				
				if (empty($_POST['newsletters_woocommerce_removeold'])) {
					$this -> delete_option('newsletters_woocommerce_removeold');
				}
				
				if (empty($_POST['newsletters_woocommerce_subscriptions_display'])) {
					$this -> delete_option('newsletters_woocommerce_subscriptions_display');
				}
				
				if (empty($_POST['newsletters_woocommerce_subscriptions_deactivation'])) {
					$this -> delete_option('newsletters_woocommerce_subscriptions_deactivation');
				}
			
				return;
			}
			
			function plugin_action_links($actions = null, $plugin_file = null, $plugin_data = null, $context = null) {
				$this_plugin = plugin_basename(__FILE__);
				
				if (!empty($plugin_file) && $plugin_file == $this_plugin) {
					$actions[] = '<a href="' . wp_nonce_url('admin.php?page=' . $this -> sections -> extensions_settings) . '#newsletters_woocommerce"><i class="fa fa-cog fa-fw"></i> ' . __('Settings', 'newsletters-woocommerce') . '</a>';
				}
				
				return $actions;
			}
			
			function apply_newsletters_checkbox() {
				
				$contains_subscription = false;
				if (class_exists('WC_Subscriptions_Cart')) {
					$contains_subscription = WC_Subscriptions_Cart::cart_contains_subscription();
				}
				
				$newsletters_woocommerce_subscriptions_display = $this -> get_option('newsletters_woocommerce_subscriptions_display');
				if (!class_exists('WC_Subscriptions')) {
					$newsletters_woocommerce_subscriptions_display = false;
				}
				
				$newsletters_woocommerce_checkbox = $this -> get_option('newsletters_woocommerce_checkbox');
				if (!empty($newsletters_woocommerce_checkbox) && (empty($newsletters_woocommerce_subscriptions_display) || (!empty($newsletters_woocommerce_subscriptions_display) && $contains_subscription == true))) {
					$this -> render('views' . DS . 'subscribe', array('errors' => $errors), true, false, 'newsletters_woocommerce');
				}
			}

			function add_to_subscriber_list($order_id){


				global $woocommerce, $Subscriber;
				
				$order = new WC_Order($order_id);
				$items = $order -> get_items();
				$user_id = $order -> get_user_id();
				$is_subscribed = get_post_meta($order_id,'tribulant_newsletters_woocommerce',true);

				if(!empty($is_subscribed)){					
					
					$newsletters_woocommerce_liststype = $this -> get_option('newsletters_woocommerce_liststype');
					if (empty($newsletters_woocommerce_liststype) || $newsletters_woocommerce_liststype == "admin") {
						$newsletters_woocommerce_mailinglists = $this -> get_option('newsletters_woocommerce_mailinglists');
					} else {
						$newsletters_woocommerce_mailinglists = $is_subscribed;
					}
					
					$newsletters_woocommerce_mailinglists = apply_filters('newsletters_woocommerce_mailinglists', $newsletters_woocommerce_mailinglists); 
					
					// Product specific mailing lists?
					//if ($items = WC() -> cart -> get_cart()) {
						foreach ($items as $item) {
							$post_id = $item['product_id'];
							if (!empty($post_id)) {
								$newsletters_woocommerce_product = get_post_meta($post_id, '_newsletters_woocommerce_product', true);								
								if (!empty($newsletters_woocommerce_product)) {
									$newsletters_woocommerce_product_mailinglists = get_post_meta($post_id, '_newsletters_woocommerce_mailinglists', true);
									if (!empty($newsletters_woocommerce_product_mailinglists)) {
										$newsletters_woocommerce_mailinglists = array_unique(array_merge($newsletters_woocommerce_mailinglists, $newsletters_woocommerce_product_mailinglists));
									}
								}
							}
						}
					//}
					
					$data = array(
						'email'				=>	$order->get_billing_email(),
						'list_id'			=>	$newsletters_woocommerce_mailinglists,
						'mailinglists'		=>	$newsletters_woocommerce_mailinglists,
					);
					
					$newsletters_woocommerce_fields = $this -> get_option('newsletters_woocommerce_fields');
					if (!empty($newsletters_woocommerce_fields)) {
						foreach ($newsletters_woocommerce_fields as $field_slug => $wc_field) {
							if (!empty($wc_field)) {
								if ($wc_value = get_user_meta($user_id, $wc_field, true)) {
									$data[$field_slug] = $wc_value;	
								}
							}
						}
					}
					
					$newsletters_woocommerce_subscriptions_activation = $this -> get_option('newsletters_woocommerce_subscriptions_activation');
					if (!empty($newsletters_woocommerce_subscriptions_activation) && $newsletters_woocommerce_subscriptions_activation == "activated") {
						if (class_exists('WC_Subscriptions_Order')) {
							if (WC_Subscriptions_Order::order_contains_subscription($order_id)) {
								$data['active'] = "N";
							}
						}
					}
					
					$newsletters_woocommerce_errors = $this -> subscribe($data);
					
					if (!empty($newsletters_woocommerce_errors)) {
						foreach ($newsletters_woocommerce_errors as $error) {
							wc_add_notice($error, 'error');
						}
					} else {
						$subscriber_id = $Subscriber -> data -> id;
						$this -> SubscriberMeta() -> save(array('subscriber_id' => $subscriber_id, 'meta_key' => "_woocommerce_order_id", 'meta_value' => $order_id));
						
						if (class_exists('WC_Subscriptions_Manager')) {
							$subscription_key = WC_Subscriptions_Manager::get_subscription_key($order_id);
							$this -> SubscriberMeta() -> save(array('subscriber_id' => $subscriber_id, 'meta_key' => "_woocommerce_subscription_key", 'meta_value' => $subscription_key));	
						}
					}
					
				}
			
			}
			
			function woo_checkout_order_processed($order_id = null, $posted = null) {
				global $woocommerce, $Subscriber;
				
				$order = new WC_Order($order_id);
				$items = $order -> get_items();
				$user_id = $order -> get_user_id();
				
				if (!empty($_POST['newsletters_woocommerce'])) {
					update_post_meta($order_id,'tribulant_newsletters_woocommerce',$_POST['newsletters_woocommerce']);
				}
			}
			
			function management_subscribe($data = null) {
				global $Db, $Subscriber, $SubscribersList;
				
				if (!empty($data)) {
					$newsletters_woocommerce_removeold = $this -> get_option('newsletters_woocommerce_removeold');
					if (!empty($newsletters_woocommerce_removeold)) {
						if (!empty($data['email'])) {
							if ($subscriber = $Subscriber -> get_by_email($data['email'])) {
								$Db -> model = $SubscribersList -> model;
								$Db -> delete_all(array('subscriber_id' => $subscriber -> id));
							}
						}
					}
				}	
			}
			
			function subscribe($data = null) {				
				if (class_exists('wpMail')) {
					$wpMail = new wpMail();
					global $Db, $Subscriber, $SubscribersList;
					
					$newsletters_woocommerce_removeold = $this -> get_option('newsletters_woocommerce_removeold');
					if (!empty($newsletters_woocommerce_removeold)) {
						if (!empty($data['email'])) {
							if ($subscriber = $Subscriber -> get_by_email($data['email'])) {
								$Db -> model = $SubscribersList -> model;
								$Db -> delete_all(array('subscriber_id' => $subscriber -> id));
							}
						}
					}
					
					$Subscriber -> optin($data, false, false, true);
					return $Subscriber -> errors;
				}
			}
			
			function woo_subscription_status_updated($subscription = null, $new_status = null, $old_status = null) {				
				global $wpdb, $Db, $SubscribersList;
				
				$subscriber_id = $this -> SubscriberMeta() -> field('subscriber_id', array('meta_key' => "_woocommerce_order_id", 'meta_value' => $subscription -> order -> id));
				
				if (!empty($subscription) && !empty($new_status)) {
					switch ($new_status) {
						case 'active'				:
						case 'on-hold_to_active'	:
							$Db -> model = $SubscribersList -> model;
							$Db -> save_field('active', "Y", array('subscriber_id' => $subscriber_id));
							break;
						case 'on-hold'				:
						case 'cancelled'			:
						case 'expired'				:
							$newsletters_woocommerce_subscriptions_deactivation = $this -> get_option('newsletters_woocommerce_subscriptions_deactivation');
							if (!empty($newsletters_woocommerce_subscriptions_deactivation)) {
								$Db -> model = $SubscribersList -> model;
								$Db -> save_field('active', "N", array('subscriber_id' => $subscriber_id));
							}
							break;
						default 					:
						
							break;
					}
				}
			}
			 
			function add_newsletters_woocommerce_endpoint() {
			    add_rewrite_endpoint('newsletters', EP_ROOT | EP_PAGES );
			}
			 
			function add_newsletters_woocommerce_query_vars( $vars ) {
			    $vars[] = 'newsletters';
			    return $vars;
			}
			 
			function add_newsletters_woocommerce_link_my_account( $items ) {
			    $items['newsletters'] = 'Newsletters';
			    return $items;
			}
			 
			function add_newsletters_woocommerce_content() {
				echo '<h2>' . __('Newsletters', 'newsletters-woocommerce') . '</h2>';
				echo do_shortcode('[newsletters_management]');
			}
			
			function product_data_tabs($product_data_tabs = null) {
				
				$product_data_tabs['newsletters'] = array(
					'label'		=> __('Newsletters', 'newsletters-woocommerce'),
					'target'	=> 'newsletters_options',
					'class'		=> array('show_if_simple', 'show_if_variable'),
				);
				
				return $product_data_tabs;
			}
			
			function product_data_panels() {
				global $post;
				
				$newsletters_woocommerce_product = get_post_meta($post -> ID, '_newsletters_woocommerce_product', true);
				$newsletters_woocommerce_mailinglists = get_post_meta($post -> ID, '_newsletters_woocommerce_mailinglists', true);
				
				?>
				
				<div id="newsletters_options" class="panel woocommerce_options_panel hidden">
					<div class="options-group">
						<p class="form-field _newsletters_woocommerce_product_field">
							<label for="_newsletters_woocommerce_product"><?php _e('Enable List/s', 'newsletters-woocommerce'); ?></label>
							<input <?php echo (!empty($newsletters_woocommerce_product)) ? 'checked="checked"' : ''; ?> onclick="if (jQuery(this).is(':checked')) { jQuery('#_newsletters_woocommerce_product_div').show(); } else { jQuery('#_newsletters_woocommerce_product_div').hide(); }" type="checkbox" name="_newsletters_woocommerce_product" value="1" id="_newsletters_woocommerce_product" />
							<span class="description"><?php _e('Set custom mailing list/s to add a customer to if they purchase this product.', 'newsletters-woocommerce'); ?></span>
						</p>
					</div>	
					
					<div class="options-group" id="_newsletters_woocommerce_product_div" style="display:<?php echo (!empty($newsletters_woocommerce_product)) ? 'block' : 'none'; ?>;">
						<p class="form-field">
							<label for=""><?php _e('Mailing List/s', 'newsletters-woocommerce'); ?></label>
							<?php if ($mailinglists = wpml_get_mailinglists(true)) : ?>
								<span style="font-weight:bold;"><input type="checkbox" class="checkbox" name="checkall" value="checkall" id="checkall" onclick="jqCheckAll(true, false, '_newsletters_woocommerce_mailinglists');" /> <?php _e('Select All', 'newsletters-woocommerce'); ?></span><br/>
								<?php foreach ($mailinglists as $mailinglist) : ?>
									<input <?php echo (!empty($newsletters_woocommerce_mailinglists) && in_array($mailinglist -> id, $newsletters_woocommerce_mailinglists)) ? 'checked="checked"' : ''; ?> type="checkbox" class="checkbox" name="_newsletters_woocommerce_mailinglists[]" value="<?php echo esc_attr(stripslashes($mailinglist -> id)); ?>" id="_newsletters_woocommerce_mailinglists_<?php echo $mailinglist -> id; ?>" /> <?php _e($mailinglist -> title); ?><br/>
								<?php endforeach; ?>
							<?php else : ?>
								<span class="newsletters-error"><?php _e('No mailing lists are available.', 'newsletters-woocommerce'); ?></span>
							<?php endif; ?>
						</p>
					</div>
				</div>
				
				<style type="text/css">
				#woocommerce-product-data ul.wc-tabs li.newsletters_tab a::before {
				    font-family: 'FontAwesome' !important;
				    content: "\f0e0" !important;
				    -webkit-font-smoothing: antialiased;
				    -moz-osx-font-smoothing: grayscale;
				}
				</style>
				
				<?php
			}
			
			function process_product_meta_simple($post_id = null) {
				
				$newsletters_woocommerce_product = sanitize_text_field($_POST['_newsletters_woocommerce_product']);
				if (!empty($newsletters_woocommerce_product)) {
					update_post_meta($post_id, '_newsletters_woocommerce_product', 1);
					$newsletters_woocommerce_mailinglists = map_deep($_POST['_newsletters_woocommerce_mailinglists'], 'sanitize_text_field');
					if (!empty($newsletters_woocommerce_mailinglists)) {
						update_post_meta($post_id, '_newsletters_woocommerce_mailinglists', $newsletters_woocommerce_mailinglists);
					}
				}
			}
		}
		
		if (!function_exists('NEWS_WOO')) {
			function NEWS_WOO($param = null) {
				return new newsletters_woocommerce($param);
			}
		}
		
		$plugin = plugin_basename(__FILE__);
		$newsletters_woocommerce = new newsletters_woocommerce();
		register_activation_hook(__FILE__, array($newsletters_woocommerce, 'activation_hook'));
		
		// WordPress hooks
		add_action('init', array($newsletters_woocommerce, 'init_textdomain'), 10, 1);
		add_action('init', array($newsletters_woocommerce, 'add_newsletters_endpoint'), 10, 1);
		add_action('after_plugin_row_' . $plugin, array($newsletters_woocommerce, 'after_plugin_row'), 10, 2);
		add_action('install_plugins_pre_plugin-information', array($newsletters_woocommerce, 'display_changelog'), 10, 1);
		add_filter('transient_update_plugins', array($newsletters_woocommerce, 'check_update'), 10, 1);
		add_filter('site_transient_update_plugins', array($newsletters_woocommerce, 'check_update'), 10, 1);
		add_filter('plugin_action_links', array($newsletters_woocommerce, 'plugin_action_links'), 10, 4);
		
		// Newsletter plugin hooks
		add_action('wpml_metaboxes_extensions_settings', array($newsletters_woocommerce, 'metaboxes_extensions_settings'), 10, 1);
		add_action('wpml_contacts_before_register_button', array($newsletters_woocommerce, 'contacts_before_register_button'), 10, 2);
		add_action('wpml_shipping_after_global', array($newsletters_woocommerce, 'shipping_after_global'), 10, 2);
		add_action('wpml_billing_after_global', array($newsletters_woocommerce, 'billing_after_global'), 10, 2);
		add_filter('wpml_extensions_list', array($newsletters_woocommerce, 'extensions_list'), 10, 1);
		add_action('wpml_extensions_settings_saved', array($newsletters_woocommerce, 'newsletters_extensions_settings_saved'), 10, 1);
		add_action('newsletters_management_subscribe_before', array($newsletters_woocommerce, 'management_subscribe'), 10, 1);
		
		// WooCommerce plugin hooks
		add_action('woocommerce_checkout_order_processed', array($newsletters_woocommerce, 'woo_checkout_order_processed'), 10, 2); 

		add_action('woocommerce_thankyou', array($newsletters_woocommerce, 'add_to_subscriber_list'), 10, 1); 

		add_action('init', array($newsletters_woocommerce, 'add_newsletters_woocommerce_endpoint')); 
		add_filter('query_vars', array($newsletters_woocommerce, 'add_newsletters_woocommerce_query_vars'), 0 );		 
		add_filter('woocommerce_account_menu_items', array($newsletters_woocommerce, 'add_newsletters_woocommerce_link_my_account' ));		 
		add_action('woocommerce_account_newsletters_endpoint', array($newsletters_woocommerce, 'add_newsletters_woocommerce_content' ));
		add_filter('woocommerce_product_data_tabs', array($newsletters_woocommerce, 'product_data_tabs'));
		add_action('woocommerce_product_data_panels', array($newsletters_woocommerce, 'product_data_panels'));
		add_action('woocommerce_process_product_meta', array($newsletters_woocommerce, 'process_product_meta_simple'));
		
		// WooCommerce Subscriptions plugin hooks
		add_action('woocommerce_subscription_status_updated', array($newsletters_woocommerce, 'woo_subscription_status_updated'), 10, 3);	
	}
}

?>