Newsletters - WooCommerce
=========================

[Newsletters - WooCommerce Subscribers](https://tribulant.com/extensions/view/42/woocommerce-subscribers) is a free extension plugin to capture email subscribers from your [WooCommerce plugin](https://wordpress.org/plugins/woocommerce/) checkout procedure into your [Newsletter plugin](https://tribulant.com/plugins/view/1/wordpress-newsletter-plugin). Please note that our Newsletter plugin is not free, it is premium.

The extension plugin will place a simple subscribe checkbox with a label of your choice on the checkout step of the WooCommerce plugin. The customer can then tick/check this checkbox to subscribe. You have control over whether the checkbox should be auto/pre checked and which mailing list(s) the customers should be subscribed to.

Please post your suggestions and feedback in our [community forums](https://tribulant.com/forums/) and we'll be glad to consider it for development or fork it here on GitHub and send a push request.

We hope you enjoy the extension and that it will help you to do cross-marketing to your WooCommerce customers using our powerful newsletter plugin for WordPress.

Requirements
------------

- [WordPress](https://wordpress.org) 3.8+
- [Newsletter plugin](https://tribulant.com/plugins/view/1/wordpress-newsletter-plugin) 4.1+
- [WooCommerce plugin](https://wordpress.org/plugins/woocommerce/) 2.1+

Installation
------------

With the required packages installed as indicated above, download the [Newsletters - WooCommerce Subscribers](https://tribulant.com/extensions/view/42/woocommerce-subscribers) extension plugin either here on GitHub as a ZIP, by forking it to your computer or by downloading from [our site](https://tribulant.com).

1. Upload the ZIP under **Plugins > Add New** in WordPress or upload the `newsletters-woocommerce` folder to `wp-content/plugins/` in your WordPress installation.
2. Go to the **Plugins** section in your WordPress dashboard or go to **Newsletters > Extensions** in your dashboard and activate the **Newsletters - WooCommerce Subscribers** extension plugin by clicking the **Activate** link.
3. With the extension active, you can now configure it under **Newsletters > Extensions > Settings (tab)** in your WordPress dashboard.
