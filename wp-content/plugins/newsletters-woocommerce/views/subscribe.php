<!-- WooCommerce subscribe checkbox -->

<?php

$newsletters_woocommerce_liststype = $this -> get_option('newsletters_woocommerce_liststype');
$newsletters_woocommerce_checkboxautocheck = $this -> get_option('newsletters_woocommerce_checkboxautocheck');
$newsletters_woocommerce_checkboxlabel = $this -> get_option('newsletters_woocommerce_checkboxlabel');
$newsletters_woocommerce_liststype_user_selection = $this -> get_option('newsletters_woocommerce_liststype_user_selection');

if (empty($newsletters_woocommerce_liststype_user_selection) || $newsletters_woocommerce_liststype_user_selection == "multiple") {
	$type = 'checkbox';
} else {
	$type = 'radio';
}

$checkbox = '';
$checkbox .= '<p class="form-row form-row-wide newsletters-woocommerce">';

if (!empty($newsletters_woocommerce_liststype) && $newsletters_woocommerce_liststype == "admin") {
	$checkbox .= '<label for="newsletters_woocommerce" class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">';
	$checkbox .= '<input ' . ((!empty($newsletters_woocommerce_checkboxautocheck) || !empty($_POST['newsletters_woocommerce'])) ? 'checked="checked"' : '') . ' class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" id="newsletters_woocommerce" type="checkbox" name="newsletters_woocommerce" value="1" />';
	$checkbox .= '<span>' . __($newsletters_woocommerce_checkboxlabel) . '</span>';
	$checkbox .= '</label>';
} else {
	if ($mailinglists = wpml_get_mailinglists()) {
		$checkbox .= __($newsletters_woocommerce_checkboxlabel);
		
		if (!empty($newsletters_woocommerce_liststype_user_selection) && $newsletters_woocommerce_liststype_user_selection == "single") {			
			foreach ($mailinglists as $mailinglist) {
				$checkbox .= '<label for="newsletters_woocommerce_' . $mailinglist -> id . '" class="woocommerce-form__label woocommerce-form__label-for-radio radio">';
				$checkbox .= '<input ' . ((!empty($_POST['newsletters_woocommerce']) && in_array($mailinglist -> id, $_POST['newsletters_woocommerce'])) ? 'checked="checked"' : '') . ' class="woocommerce-form__input woocommerce-form__input-radio input-radio" id="newsletters_woocommerce_' . $mailinglist -> id . '" type="radio" name="newsletters_woocommerce[]" value="' . $mailinglist -> id . '" />';
				$checkbox .= '<span>' . __($mailinglist -> title) . '</span>';
				$checkbox .= '</label>';
			}
		} else {
			foreach ($mailinglists as $mailinglist) {
				$checkbox .= '<label for="newsletters_woocommerce_' . $mailinglist -> id . '" class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">';
				$checkbox .= '<input ' . ((!empty($_POST['newsletters_woocommerce']) && in_array($mailinglist -> id, $_POST['newsletters_woocommerce'])) ? 'checked="checked"' : '') . ' class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" id="newsletters_woocommerce_' . $mailinglist -> id . '" type="checkbox" name="newsletters_woocommerce[]" value="' . $mailinglist -> id . '" />';
				$checkbox .= '<span>' . __($mailinglist -> title) . '</span>';
				$checkbox .= '</label>';
			}
		}
	}
}

$checkbox .= '</p>';
$checkbox .= '<div class="clear"></div>';
echo stripslashes($checkbox);

?>