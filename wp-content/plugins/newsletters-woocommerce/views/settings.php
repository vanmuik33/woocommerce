<!-- WooCommerce Subscribers Settings -->

<?php

$newsletters_woocommerce_checkbox = $this -> get_option('newsletters_woocommerce_checkbox');
$newsletters_woocommerce_checkboxautocheck = $this -> get_option('newsletters_woocommerce_checkboxautocheck');
$newsletters_woocommerce_checkboxlabel = $this -> get_option('newsletters_woocommerce_checkboxlabel');
$newsletters_woocommerce_fields = $this -> get_option('newsletters_woocommerce_fields');
$newsletters_woocommerce_liststype = $this -> get_option('newsletters_woocommerce_liststype');
$newsletters_woocommerce_liststype_user_selection = $this -> get_option('newsletters_woocommerce_liststype_user_selection');
$newsletters_woocommerce_mailinglists = $this -> get_option('newsletters_woocommerce_mailinglists');
$newsletters_woocommerce_removeold = $this -> get_option('newsletters_woocommerce_removeold');
$newsletters_woocommerce_checkbox_action = $this -> get_option('newsletters_woocommerce_checkbox_action');

WC() -> session = new WC_Session_Handler;
WC() -> customer = new WC_Customer;

?>

<table class="form-table">
	<tbody>
		<tr>
			<th><label for="newsletters_woocommerce_checkbox"><?php _e('Enable?', 'newsletters-woocommerce'); ?></label></th>
			<td>
				<label><input onclick="if (jQuery(this).is(':checked')) { jQuery('#newsletters_woocommerce_div').show(); } else { jQuery('#newsletters_woocommerce_div').hide(); }" <?php echo (!empty($newsletters_woocommerce_checkbox)) ? 'checked="checked"' : ''; ?> type="checkbox" name="newsletters_woocommerce_checkbox" value="1" id="newsletters_woocommerce_checkbox" /> <?php _e('Yes, enable the subscribe checkbox on the WooCommerce checkout', 'newsletters-woocommerce'); ?></label>
				<span class="howto"><?php _e('Tick/check this to enable the integration', 'newsletters-woocommerce'); ?></span>
			</td>
		</tr>
	</tbody>
</table>

<div id="newsletters_woocommerce_div" style="display:<?php echo (!empty($newsletters_woocommerce_checkbox)) ? 'block' : 'none'; ?>;">
	<table class="form-table">
		<tbody>
			<tr>
				<th><label for="newsletters_woocommerce_checkboxautocheck"><?php _e('Auto Check?', 'newsletters-woocommerce'); ?></label></th>
				<td>
					<label><input <?php echo (!empty($newsletters_woocommerce_checkboxautocheck)) ? 'checked="checked"' : ''; ?> type="checkbox" name="newsletters_woocommerce_checkboxautocheck" value="1" id="newsletters_woocommerce_checkboxautocheck" /> <?php _e('Yes, auto check the checkbox', 'newsletters-woocommerce'); ?></label>
					<span class="howto"><?php _e('By ticking/checking this, the subscribe checkbox will be automatically pre-checked for customers.', $this-> extension_name); ?></span>
				</td>
			</tr>
			<tr>
				<th><label for="newsletters_woocommerce_checkboxlabel"><?php _e('Checkbox Label', 'newsletters-woocommerce'); ?></label></th>
				<td>
					<?php if ($this -> language_do() && $languages = $this -> language_getlanguages()) : ?>
						<div id="newsletters-woocommerce-checkboxlabel-tabs">
								<ul>
									<?php foreach ($languages as $language) : ?>
										<li><a href="#newsletters-woocommerce-checkboxlabel-tabs-<?php echo $language; ?>"><?php echo $this -> language_flag($language); ?></a></li>
									<?php endforeach; ?>
								</ul>
								<?php foreach ($languages as $language) : ?>
									<div id="newsletters-woocommerce-checkboxlabel-tabs-<?php echo $language; ?>">
										<input type="text" class="widefat" name="newsletters_woocommerce_checkboxlabel[<?php echo $language; ?>]" value="<?php echo esc_attr(stripslashes($this -> language_use($language, $newsletters_woocommerce_checkboxlabel))); ?>" id="newsletters_woocommerce_checkboxlabel_<?php echo $language; ?>" />
									</div>
								<?php endforeach; ?>
							</div>
							
							<script type="text/javascript">
							jQuery(document).ready(function() {
								if (jQuery.isFunction(jQuery.fn.tabs)) {
									jQuery('#newsletters-woocommerce-checkboxlabel-tabs').tabs();
								}
							});
							</script>
					<?php else : ?>
						<input type="text" class="widefat" name="newsletters_woocommerce_checkboxlabel" value="<?php echo esc_attr(stripslashes($newsletters_woocommerce_checkboxlabel)); ?>" id="newsletters_woocommerce_checkboxlabel" />
					<?php endif; ?>
					<span class="howto"><?php _e('Specify the caption/label to show next to the checkbox.', 'newsletters-woocommerce'); ?></span>
				</td>
			</tr>
			<tr>
				<th><label for=""><?php _e('Custom Fields', 'newsletters-woocommerce'); ?></label></th>
				<td>
					<p class="howto"><?php _e('Assign WooCommerce checkout fields to save to the Newsletter plugin custom fields as the customer is subscribed.', 'newsletters-woocommerce'); ?></p>
					
					<?php if ($wc_fields = WC() -> checkout -> get_checkout_fields()) : ?>					
						<?php if ($fields = wpml_get_fields()) : ?>
							<table class="form-table">
								<?php foreach ($fields as $field) : ?>
									<?php if ($field -> slug != "email" && $field -> slug != "list") : ?>
										<tr>
											<th>
												<?php _e($field -> title); ?> from WooCommerce: 
											</th>
											<td>
												<select name="newsletters_woocommerce_fields[<?php echo $field -> slug; ?>]">
													<option value=""><?php _e('- Select -', 'newsletters-woocommerce'); ?></option>
													<?php foreach ($wc_fields as $wc_section => $wc_section_fields) : ?>
														<?php foreach ($wc_section_fields as $wc_section_field_key =>  $wc_section_field) : ?>
															<option <?php echo (!empty($newsletters_woocommerce_fields[$field -> slug]) && $newsletters_woocommerce_fields[$field -> slug] == $wc_section_field_key) ? 'selected="selected"' : ''; ?> value="<?php echo $wc_section_field_key; ?>"><?php echo __($wc_section_field['label']); ?></option>
														<?php endforeach; ?>
													<?php endforeach; ?>
												</select>
											</td>
										</tr>
									<?php endif; ?>
								<?php endforeach; ?>
							</table>
						<?php endif; ?>
					<?php else : ?>
						<p class="newsletters_error"><?php _e('No checkout fields were found', 'newsletters-woocommerce'); ?></p>
					<?php endif; ?>
				</td>
			</tr>
			<tr>
				<th><label for="newsletters_woocommerce_liststype_admin"><?php _e('Mailing List/s', 'newsletters-woocommerce'); ?></label></th>
				<td>
					<p>
						<label><input onclick="jQuery('#newsletters_woocommerce_liststype_div').show(); jQuery('#newsletters_woocommerce_liststype_user_div').hide();" <?php echo (empty($newsletters_woocommerce_liststype) || (!empty($newsletters_woocommerce_liststype) && $newsletters_woocommerce_liststype == "admin")) ? 'checked="checked"' : ''; ?> type="radio" name="newsletters_woocommerce_liststype" value="admin" id="newsletters_woocommerce_liststype_admin" /> <?php _e('Admin Defined', 'newsletters-woocommerce'); ?></label>
						<label><input onclick="jQuery('#newsletters_woocommerce_liststype_div').hide(); jQuery('#newsletters_woocommerce_liststype_user_div').show();" <?php echo (!empty($newsletters_woocommerce_liststype) && $newsletters_woocommerce_liststype == "user") ? 'checked="checked"' : ''; ?> type="radio" name="newsletters_woocommerce_liststype" value="user" id="newsletters_woocommerce_liststype_user" /> <?php _e('User Choice', 'newsletters-woocommerce'); ?></label>
					</p>
					
					<div id="newsletters_woocommerce_liststype_div" style="display:<?php echo (!empty($newsletters_woocommerce_liststype) && $newsletters_woocommerce_liststype == "admin") ? 'block' : 'none'; ?>;">
						<?php if ($mailinglists = wpml_get_mailinglists(true)) : ?>
							<label style="font-weight:bold;"><input type="checkbox" name="checkboxall" value="1" id="checkboxall" onclick="jqCheckAll(true, false, 'newsletters_woocommerce_mailinglists');" /> <?php _e('Select All', 'newsletters-woocommerce'); ?></label>
							<div class="scroll-list">
								<?php foreach ($mailinglists as $mailinglist) : ?>
									<label><input <?php echo (!empty($newsletters_woocommerce_mailinglists) && in_array($mailinglist -> id, $newsletters_woocommerce_mailinglists)) ? 'checked="checked"' : ''; ?> type="checkbox" name="newsletters_woocommerce_mailinglists[]" value="<?php echo $mailinglist -> id; ?>" id="newsletters_woocommerce_mailinglists_<?php echo $mailinglist -> id; ?>" /> <?php _e($mailinglist -> title); ?></label><br/>	
								<?php endforeach; ?>
							</div>
							<span class="howto"><?php _e('Choose the list(s) to which the customers will be subscribed to.', 'newsletters-woocommerce'); ?></span>
						<?php else : ?>
							<span class="newsletters_error"><?php _e('No mailing lists are available', 'newsletters-woocommerce'); ?></span>
						<?php endif; ?>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	
	<div id="newsletters_woocommerce_liststype_user_div" style="display:<?php echo (!empty($newsletters_woocommerce_liststype) && $newsletters_woocommerce_liststype == "user") ? 'block' : 'none'; ?>;">
		<table class="form-table">
			<tbody>
				<tr>
					<th><label for="newsletters_woocommerce_liststype_user_selection_multiple"><?php _e('Selection', 'newsletters-woocommerce'); ?></label></th>
					<td>
						<label><input <?php echo (!empty($newsletters_woocommerce_liststype_user_selection) && $newsletters_woocommerce_liststype_user_selection == "multiple") ? 'checked="checked"' : ''; ?> type="radio" name="newsletters_woocommerce_liststype_user_selection" value="multiple" id="newsletters_woocommerce_liststype_user_selection_multiple" /> <?php _e('Multiple (Checkboxes)', 'newsletters-woocommerce'); ?></label>
						<label><input <?php echo (!empty($newsletters_woocommerce_liststype_user_selection) && $newsletters_woocommerce_liststype_user_selection == "single") ? 'checked="checked"' : ''; ?> type="radio" name="newsletters_woocommerce_liststype_user_selection" value="single" id="newsletters_woocommerce_liststype_user_selection_single" /> <?php _e('Single (Radio)', 'newsletters-woocommerce'); ?></label>
						<span class="howto"><?php _e('Should users be able to select multiple or only a single mailing list?', 'newsletters-woocommerce'); ?></span>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	
	<table class="form-table">
		<tbody>
			<tr>
				<th><label for="newsletters_woocommerce_removeold"><?php _e('Remove Other Subscriptions?', 'newsletters-woocommerce'); ?></label></th>
				<td>
					<label><input <?php echo (!empty($newsletters_woocommerce_removeold)) ? 'checked="checked"' : ''; ?> type="checkbox" name="newsletters_woocommerce_removeold" value="1" id="newsletters_woocommerce_removeold" /> <?php _e('Yes, when a customer subscribes, remove previous/old subscriptions.', 'newsletters-woocommerce'); ?></label>
				</td>
			</tr>
		</tbody>
	</table>
	
	<h3><?php _e('Advanced Checkbox Settings', 'newsletters-woocommerce'); ?></h3>
	<table class="form-table">
		<tbody>
			<tr>
				<th><label for="newsletters_woocommerce_checkbox_action"><?php _e('WooCommerce Form Action', 'newsletters-woocommerce'); ?></label></th>
				<td>
					<input type="text" class="widefat" name="newsletters_woocommerce_checkbox_action" value="<?php echo esc_attr(stripslashes($newsletters_woocommerce_checkbox_action)); ?>" id="newsletters_woocommerce_checkbox_action" />
					<span class="howto"><?php echo sprintf(__('You can change the location of the subscribe checkbox, see the %s'), '<a href="https://docs.woocommerce.com/wc-apidocs/hook-docs.html" target="_blank">' . __('available WooCommerce form actions', 'newsletters-woocommerce') . '</a>'); ?></span>
				</td>
			</tr>
		</tbody>
	</table>
	
	<h3><?php _e('WooCommerce Subscriptions', 'newsletters-woocommerce'); ?></h3>
	<?php if (class_exists('WC_Subscriptions')) : ?>
		<?php
			
		$newsletters_woocommerce_subscriptions_display = $this -> get_option('newsletters_woocommerce_subscriptions_display');
		$newsletters_woocommerce_subscriptions_activation = $this -> get_option('newsletters_woocommerce_subscriptions_activation');
		$newsletters_woocommerce_subscriptions_deactivation = $this -> get_option('newsletters_woocommerce_subscriptions_deactivation');	
			
		?>
		
		<table class="form-table">
			<tbody>
				<tr>
					<th><label for="newsletters_woocommerce_subscriptions_display"><?php _e('Display', 'newsletters-woocommerce'); ?></label></th>
					<td>
						<label><input <?php echo (!empty($newsletters_woocommerce_subscriptions_display)) ? 'checked="checked"' : ''; ?> type="checkbox" name="newsletters_woocommerce_subscriptions_display" value="1" id="newsletters_woocommerce_subscriptions_display" /> <?php _e('Only display subscribe checkbox(es) when order has subscription product.', 'newsletters-woocommerce'); ?></label>
						<span class="howto"><?php _e('Choose this to only show subscribe options for subscription orders. Else subscribe options will always show.', 'newsletters-woocommerce'); ?></span>
					</td>
				</tr>
				<tr>
					<th><label for="newsletters_woocommerce_subscriptions_activation_always"><?php _e('Activation', 'newsletters-woocommerce'); ?></label></th>
					<td>
						<label><input <?php echo (empty($newsletters_woocommerce_subscriptions_activation) || (!empty($newsletters_woocommerce_subscriptions_activation) && $newsletters_woocommerce_subscriptions_activation == "always")) ? 'checked="checked"' : ''; ?> type="radio" name="newsletters_woocommerce_subscriptions_activation" value="always" id="newsletters_woocommerce_subscriptions_activation_always" /> <?php _e('Always Activate', 'newsletters-woocommerce'); ?></label>
						<label><input <?php echo (!empty($newsletters_woocommerce_subscriptions_activation) && $newsletters_woocommerce_subscriptions_activation == "activated") ? 'checked="checked"' : ''; ?> type="radio" name="newsletters_woocommerce_subscriptions_activation" value="activated" id="newsletters_woocommerce_subscriptions_activation_activated" /> <?php _e('Activate Once Subscription is Activated', 'newsletters-woocommerce'); ?></label>
					</td>
				</tr>
				<tr>
					<th><label for="newsletters_woocommerce_subscriptions_deactivation"><?php _e('Deactivation', 'newsletters-woocommerce'); ?></label></th>
					<td>
						<label><input <?php echo (!empty($newsletters_woocommerce_subscriptions_deactivation)) ? 'checked="checked"' : ''; ?> type="checkbox" name="newsletters_woocommerce_subscriptions_deactivation" value="1" id="newsletters_woocommerce_subscriptions_deactivation" /> <?php _e('Unsubscribe/deactivate upon subscription cancelled/stopped.', 'newsletters-woocommerce'); ?></label>
					</td>
				</tr>
			</tbody>
		</table>
	<?php else : ?>
		<p class="newsletters_warning"><?php _e('Install WooCommerce Subscriptions plugin for further functionality.', 'newsletters-woocommerce'); ?></p>
	<?php endif; ?>
</div>
